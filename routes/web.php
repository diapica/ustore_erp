<?php
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get ('/', 'HomeController@index');

// Accounts
Route::get ('/accounts/logout', 'Auth\LoginController@logout');
Route::post('/accounts/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get ('/accounts/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/accounts/password/reset', 'Auth\ResetPasswordController@reset');
Route::get ('/accounts/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::get ('/accounts/password/change','HomeController@showChangePasswordForm');
Route::post('/accounts/password/change','HomeController@changePassword');

//Presence
Route::get ('/evaluation/presence', 'HumanResource\PresenceController@index');
Route::get ('/evaluation/presence/overview', 'HumanResource\PresenceController@overview');
Route::get ('/evaluation/presence/validate', 'HumanResource\PresenceController@validation');
Route::post('/evaluation/presence/validate', 'HumanResource\PresenceController@action');

//Profile
Route::get ('/profile', 'ProfileController@editProfile');
Route::post('/saveProfile', 'ProfileController@saveProfile');
Route::post('/imageUpload', 'ProfileController@imageUpload');

//Vendor Control Panel
Route::get ('/vendor', 'VendorController@index');
Route::get ('/vendor/create', 'VendorController@create');
Route::post('/vendor/create', 'VendorController@store');
Route::get ('/vendor/edit/{id}', 'VendorController@edit');
Route::put ('/vendor/update/{id}', 'VendorController@update');
Route::delete('/vendor/destroy/{id}','VendorController@destroy');

//Products
Route::group(['prefix'=>"",'middleware' => ['auth','warehouse']],function()
{
	Route::get ('/products','ProductsController@index');
	Route::post('/products/save','ProductsController@save');
	Route::get ('/products/{id}/{name}','ProductsController@productDetail');
	Route::get ('/products/delete/product/{id}','ProductsController@deleteProduct');
	Route::get ('/products/barcode/print/{id}','ProductsController@printBarcode');

	//Category
	Route::get ('/category','CategoryController@index');
	Route::post('/category/save','CategoryController@save');
	Route::get ('/category/delete/{id}','CategoryController@delete');
});

/*
 * Customer API
 * ---
 * 
 * Customer API can be used to register, read or authenticate user
 * from / to the system.
 * 
 */
Route::post('/api/customer/find','Api\UserAPI@findCustomer');
Route::post('/api/customer/add','Api\UserAPI@addCustomer');

/*
 * Product API
 * ---
 * 
 * Product API can be used to register, or read product from / into
 * the system
 * 
 */
Route::post('/api/product/find','Api\POSAPI@POSfindProduct');

/*
 * Point of Sales API
 * ---
 * 
 * Manage user cart and purchases
 * 
 */
Route::post('/api/pos/cart/add','PurchaseController@addProductCart');
Route::post('/api/pos/cart/modify/{id}/{status}','purchaseController@plusMinusItem');
Route::post('/api/pos/cart/refresh','PurchaseController@refreshCart');
Route::post('/api/pos/cart/clear','PurchaseController@clearCart');
Route::get('/api/pos/cart/checkout/{data}','PurchaseController@proses');

/*
 * Point of Sales Web View
 * ---
 * 
 * This is the web view of point of sales
 * 
 */
Route::get('/pos/sales','PurchaseController@index');
Route::get('/pos/sales/history','PurchaseController@salesHistory');
Route::get('/pos/sales/report','PurchaseController@salesReport');

/*
	*Print Products Barcode 
*/

//News
Route::get('/news','NewsController@index');
Route::post('/news','NewsController@store');
Route::delete('/news/{news}','NewsController@destroy');
Route::put('/news/{news}','NewsController@update');