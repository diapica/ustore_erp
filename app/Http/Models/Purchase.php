<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $table = "purchase";
    protected $fillable = ['staff_id','customer_id','subtotal','disc','gtotal','tax'];

    public function purchaseDetail(){
    	return $this->hasMany(Purchase_Detail::class);
    }

    public function staff(){
    	return $this->belongsTo(User::class,'staff_id');
    }

    public function customer(){
    	return $this->belongsTo(User::class,'customer_id');
    }

    public static function sumDiscPerPeriod($timeUnit = 'DATE_FORMAT(purchase.created_at,"%Y-%m")'){
        return self::selectRaw($timeUnit.' as "time_unit", sum(disc) as "total_disc" ')
                    ->groupBy('time_unit')
                    ->orderBY('created_at');
    }
}
