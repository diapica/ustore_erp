<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase_Detail extends Model
{
    protected $table = 'purchase_detail';
    protected $fillable = ['purchase_id','product_id','qty','price','total'];

    
    public function Product(){
    	/*return $this->belongsTo('App\Http\Models\Products','product_id');*/
    	return $this->belongsTo(Products::class);
    }

    public function Purchase(){
    	/*return $this->belongsTo('App\Http\Models\Purchase','purchase_id');*/
    	return $this->belongsTo(Purchase::class);
    }
}
