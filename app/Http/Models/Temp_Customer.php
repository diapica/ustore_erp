<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Temp_Customer extends Model
{
    protected $table = 'temp_customer';
    protected $fillable = ['nim','full_name','email'];
}
