<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = "products";
    protected $fillable = ['barcode','product_name','image','price','desc','vendor_id','category_id','stock','status'];

    public function category(){
    	return $this->belongsTo(Category::class,'category_id');
    }

    public static function sumPerPeriod($timeUnit = 'DATE_FORMAT(purchase.created_at,"%Y-%m")'){
    	return self::join('category','products.category_id','=','category.id')
    				->join('purchase_detail','purchase_detail.product_id','=','products.id')
    				->join('purchase','purchase_detail.purchase_id','=','purchase.id')
    				->selectRaw('category, product_name,products.price as "Product Price",purchase_detail.qty as "Qty", products.price*purchase_detail.qty as "Total",  purchase_detail.id as "Purchase Detail ID", purchase.id as "Purchase ID", '.$timeUnit.' as "time_unit", sum(products.price*purchase_detail.qty) as "net", sum(purchase_detail.qty) as "total_qty", sum(purchase.disc) as "total_disc"')
    				->groupBy('time_unit')
    				->orderBY('purchase.created_at');
    				/*->get();*/

    	/*return self::from('products as p')
    				->join('category as c','p.category_id','=','c.id')
    				->join('purchase_detail as pd','pd.product_id','=','p.id')
    				->join('purchase as pc','pd.purchase_id','=','pc.id')
    				->selectRaw('category, product_name,p.price as "Product Price",pd.qty as "Qty", p.price*pd.qty as "Total",  pd.id as "Purchase Detail ID", pc.id as "Purchase ID", monthname(pc.created_at) as "month", year(pc.created_at) as "year", sum(p.price*pd.qty) as "net"')
    				->groupBy('month','year')
    				->orderBY('pc.created_at')
    				->get();*/
    }

    public static function sumPerCategory($timeUnit = 'DATE_FORMAT(purchase.created_at,"%Y-%m")'){
    	
        return self::join('category','products.category_id','=','category.id')
                    ->join('purchase_detail','purchase_detail.product_id','=','products.id')
                    ->join('purchase','purchase_detail.purchase_id','=','purchase.id')
                    ->selectRaw('category, product_name,products.price as "Product Price",purchase_detail.qty as "Qty", products.price*purchase_detail.qty as "Total",  purchase_detail.id as "Purchase Detail ID", purchase.id as "Purchase ID", '.$timeUnit.' as "time_unit", sum(products.price*purchase_detail.qty) as "net", sum(purchase_detail.qty) as "total_qty", sum(purchase.disc) as "total_disc"')
                    ->groupBy('time_unit','category')
                    ->orderBY('purchase.created_at');
    }

    public static function sumPerProduct($timeUnit = 'DATE_FORMAT(purchase.created_at,"%Y-%m")'){

        return self::join('category','products.category_id','=','category.id')
                    ->join('purchase_detail','purchase_detail.product_id','=','products.id')
                    ->join('purchase','purchase_detail.purchase_id','=','purchase.id')
                    ->selectRaw('category, product_name,products.price as "Product Price",purchase_detail.qty as "Qty", products.price*purchase_detail.qty as "Total",  purchase_detail.id as "Purchase Detail ID", purchase.id as "Purchase ID", '.$timeUnit.' as "time_unit", sum(products.price*purchase_detail.qty) as "net", sum(purchase_detail.qty) as "total_qty", sum(purchase.disc) as "total_disc"')
                    ->groupBy('time_unit','product_name')
                    ->orderBY('purchase.created_at');
    }

}
