<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Presence extends Model
{
    public $timestamps = false;
    protected $table = 'presence';
    protected $fillable = [
        'id_user',
        'tap_in',
        'tap_out',
        'report'
    ];

    public static function getAvailablePresence($user)
    {
        return static::where('id_user' , '=', $user)
        ->whereNotNull('tap_in')
        ->whereNull('tap_out')
        ->get();
    }

    public static function getTodayPresence($user)
    {
        return static::where('id_user' , '=', $user)
        ->whereDate('tap_in', date('Y-m-d'))
        ->get();
    }

    public static function getAllPresence($user)
    {
        return static::where('id_user' , '=', $user)
        ->orderBy('tap_in', 'desc')
        ->get();
    }

    public function user()
    {
    	return $this->belongsTo(User::class, 'id_user');
    }
}
