<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable = ['barcode','name','stock','price'];
}
