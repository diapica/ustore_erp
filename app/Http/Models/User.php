<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = "users";
    protected $fillable = [
        'name', 'email', 'password', 'nim', 'fakultas', 'jurusan', 'angkatan'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function findUser($nim = '', $email = '')
    {
        if ($nim == '' && $email == '') return 0;
        $query = static::select('nim', 'email')
        ->where('nim' , '=', $nim)
        ->orWhere('email', '=', $email);
        if ($query->count() > 0) return $query->get();
        else return 0;
    }
}
