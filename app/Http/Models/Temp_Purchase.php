<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Temp_Purchase extends Model
{
    protected $table = "temp_purchase";
    protected $fillable = ['staff_id','customer_id'];
}
