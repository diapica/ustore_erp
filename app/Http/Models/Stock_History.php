<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Stock_History extends Model
{
    protected $table = 'stock_history';
    protected $fillable = ['product_id','user_id','before','after'];

    public function Users(){
    	return $this->belongsTo('App\Http\Models\User','user_id');
    }

}
