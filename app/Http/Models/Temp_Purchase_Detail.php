<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Temp_Purchase_Detail extends Model
{
    protected $table = 'temp_purchase_detail';
    protected $fillable = ['temp_purchase_id','product_id','qty','price'];

    public function Product(){
    	return $this->belongsTo('App\Http\Models\Products','product_id');
    }
}
