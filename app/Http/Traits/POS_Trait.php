<?php

namespace App\Http\Traits;

/*
 * Models
 * ---
 * 
 * All models nessecary for this controller
 * 
 */
use App\Http\Models\Temp_Purchase;
use App\Http\Models\Temp_Purchase_Detail;
use App\Http\Models\Purchase;
use App\Http\Models\Purchase_Detail;

trait POS_Trait
{
	public function __construct(Temp_Purchase $Temp_Purchase,Temp_Purchase_Detail $Temp_Purchase_Detail,Purchase $Purchase,Purchase_Detail $Purchase_Detail){
		$this->temp_purchase = $Temp_Purchase;
		$this->temp_purchase_detail = $Temp_Purchase_Detail;
		$this->purchase = $Purchase;
    	$this->purchase_detail = $Purchase_Detail;
	}

	public function selectTempPurchaseDetailFromTempID($temp_purchase_id){
		return $this->temp_purchase_detail->where('temp_purchase_id','=',$temp_purchase_id);

	}

	public function SelectTempPurchaseDetail($temp_purchase_detail_id){
		return $this->temp_purchase_detail->find($temp_purchase_detail_id);
	}

	public function SelectPurchaseDetail($purchase_id){
		return $this->purchase_detail->where('purchase_id','=',$purchase_id);
	}

	public function CreateTempPurchase($arr_temp_purchase){
		return $this->temp_purchase->create($arr_temp_purchase);
	}

	public function CreateTempPurchaseDetail($arr_temp_purchase_detail){
		$this->temp_purchase_detail->create($arr_temp_purchase_detail);
		return;
	}

	public function CreatePurchase($arr_purchase){
		return $this->purchase->create($arr_purchase);
	}

	public function CreatePurchaseDetail($arr_purchase_detail){
		return $this->purchase_detail->create($arr_purchase_detail);	
	}

	public function UpdateTempPurchaseDetail($temp_purchase_detail_id,$arr_temp_purchase_detail){
		$this->temp_purchase_detail->find($temp_purchase_detail_id)->update($arr_temp_purchase_detail);
	}

	public function FindTempPurchaseFromCustomerID($customer_id){
		return $this->temp_purchase->where("customer_id",'=',$customer_id);
	}

	public function FindTempPurchase($temp_purchase_id){
		return $this->temp_purchase->find($temp_purchase_id);
	}

	public function UpdatePurchase($purchase_id,$arr_purchase){
		return $this->purchase->find($purchase_id)->update($arr_purchase);
	}


	public function CheckTempDetail($product_id,$temp_purchase_id){
	 	return $this->temp_purchase_detail->where('product_id','=',$product_id)->where('temp_purchase_id','=',$temp_purchase_id);
	}

	public function CheckStockEnough($stock,$qty){

		// Return True Or False
		 if($stock < $qty ){
		 	return "0";
		 }else{
		 	return "1";
		 }
	}
}