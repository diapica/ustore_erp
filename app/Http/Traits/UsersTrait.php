<?php

namespace App\Http\Traits;


/*
 * Models
 * ---
 * 
 * All models nessecary for this controller
 * 
 */
Use App\Http\Models\User;

trait UsersTrait
{
	public function consturct(User $User){
		$this->user = $User;
	}

	public function GetUserID($nim){
		$user_id = $this->user->where("nim",'=',$nim)->value("id");
		return $user_id;
	}
}