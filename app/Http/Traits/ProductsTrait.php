<?php 

namespace App\Http\Traits;

/*
 * Models
 * ---
 * 
 * All models nessecary for this controller
 * 
 */
Use App\Http\Models\Products;


trait ProductsTrait
{

	public function __construct(Products $Products){
		$this->products = $Products;
    }

    public function updateProduct($product_id,$arr_product){
    	$this->products->find($product_id)->update($arr_product);
    	return;
    }

    public function showProductsNotZero(){

    	$products_list = $this->products->where('stock',"!=",'0')->get();

    	return $products_list;
    }

    public function findProduct($search_text,$type)
    {	

        if($type=="name"){
    	   $products_list = $this->products->where('product_name','like','%'.$search_text.'%')->where('stock',"!=",'0')->get();
        }else if($type == "barcode"){
            $products_list = $this->products->where('barcode','=',$search_text)->where('stock',"!=",'0')->get();
        }else if($type == "product_id"){
        	$products_list = $this->products->find($search_text);
        }

    	return $products_list;

    }

}