<?php

namespace App\Http\Controllers;

/*
 * Modules Dependencies
 * ---
 * 
 * All functional dependencies for this controller
 * 
 */
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/*
 * Models
 * ---
 * 
 * All models nessecary for this controller
 * 
 */
use App\Http\Models\Category; 

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
