<?php

namespace App\Http\Controllers;

/*
 * Modules Dependencies
 * ---
 * 
 * All functional dependencies for this controller
 * 
 */
use Illuminate\Http\Request;

/*
 * Models
 * ---
 * 
 * All models nessecary for this controller
 * 
 */
use App\Http\Models\Category;


class CategoryController extends Controller
{
	public function __construct(Category $category)
	{
		$this->middleware('auth');
    	$this->category = $category;
    }

    public function index(){
    	$categories = $this->category->all();
    	return view('/static/warehouse/Category',[
    		'categories'=>$categories
    	]);
    }

    public function save(Request $request){
    	$category = $request->input('category');
    	$cmd = $request->input('cmd');

    	$params = [
    		'category'=>$category
    	];
    	if($cmd=="Add Category"){
    		$this->category->create($params);    		
    	}else{
    		$id = $request->input('sembunyi');
    		$this->category->find($id)->update($params);
    	}
    	return redirect()->back();
    }

    public function delete($id){
    	$this->category->find($id)->delete();
    	return redirect()->back();
    }
}
