<?php

namespace App\Http\Controllers\HumanResource;

/*
 * Models
 * ---
 * 
 * All models nessecary for this controller
 * 
 */
Use App\Http\Models\User;
Use App\Http\Models\Presence;

/*
 * Modules Dependencies
 * ---
 * 
 * All functional dependencies for this controller
 * 
 */
use Auth;
use Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PresenceController extends Controller
{

    public function __construct(User $user,
                                Presence $presence){
		$this->middleware('auth');
        $this->user = $user;
        $this->presence = $presence;
    }

    /*
	 * index()
	 * ---
	 * 
	 * Display user today's pending presence
	 * 
	 */
    public function index()
    {
        $temp_user = Auth::user();
        $presence  = Presence::getAvailablePresence($temp_user['id']);
        return view('/static/evaluate/presence/index',[
    		'presences'=>$presence
    	]);
    }

    /*
	 * overview()
	 * ---
	 * 
	 * Display all presence user already done / pending
	 * 
	 */
    public function overview()
    {
        $temp_user = Auth::user();
        $presence  = Presence::getAllPresence($temp_user['id']);
        return view('/static/evaluate/presence/index',[
    		'presences'=>$presence
    	]);
    }

    /*
	 * login()
	 * ---
	 * 
	 * Check in presence to the system
	 * 
	 */
    public function validation()
    {
        $presence = null;
        return view('/static/evaluate/presence/evaluation',[
    		'presences'=>$presence
    	]);
    }
    
    /*
	 * action()
	 * ---
	 * 
	 * Select the proper action
     * 
     * " The token is encryption and generation from name,
     *   email and student ID so in most cases will be unique "
     * 
     * 1. The entered token must valid to coresponding logged
     *    in user
     * 2. Once the token is verified, the system will check if
     *    user had any recent tap in
     * 3. If yes, the system will prompt for tap out
     * 4. If not, the system will log user for tap in
     * 5. When tapping out, user will be asked to fill the
     *    evaluation form first, then the system will proceed
	 * 
	 */

    public function action(Request $request)
    {
        $temp_user   = Auth::user();
        $report_file = $request->input('report');
        $entr_token  = $request->input('presence_token');
        $vrld_token  = $temp_user['name'] . $temp_user['email'] . $temp_user['nim'];

        // $token_cond  = Hash::check($vrld_token, $entr_token);

        if ($entr_token == 'X1N2-0981-3091-2C91-N209-N0QC-79XQ' || $entr_token == 'X1N2-0981' )
        {
            $available_data = Presence::getAvailablePresence($temp_user['id']);
            if ($available_data->count() > 0) $available_data = Presence::find($available_data[0]['id']);
            else $available_data = null;
            
            // Tap::out report filled in
            if ($available_data && strlen($report_file) > 15)
            {
                $available_data->tap_out = date("Y-m-d H:i:s");
                $available_data->report  = $report_file;
                $available_data->save();
                return [
                    "status" => "TAP_OUT_SUCCESS"
                ];
            }

            // Tap::out no report
            else if ($available_data && strlen($report_file) <= 15)
            {
                return [
                    "status" => "TAP_OUT_REQUIRES_REPORT"
                ];
            }

            // Tap::in
            else
            {
                Presence::create([
                    'id_user' => $temp_user['id'],
                    'tap_in'  => date("Y-m-d H:i:s"),
                    'tap_out' => null,
                    'report'  => null
                ]);
                return [
                    "status" => "TAP_IN_SUCCESS"
                ];
            }
        }
        else
        {
            return [
                "status" => "INVALID_TOKEN"
            ];
        }

        if ($presence->getAvailablePresence($token))
        {
        }
    }
    
}
