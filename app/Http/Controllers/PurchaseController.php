<?php

namespace App\Http\Controllers;
/*
 * Models
 * ---
 * 
 * All models nessecary for this controller
 * 
 */
use App\Http\Models\Products;
Use App\Http\Models\User;
Use App\Http\Models\Temp_Customer;
use App\Http\Models\Temp_Purchase;
use App\Http\Models\Temp_Purchase_Detail;
use App\Http\Models\Purchase;
use App\Http\Models\Purchase_Detail;

/*
 * Modules Dependencies
 * ---
 * 
 * All functional dependencies for this controller
 * 
 */
use Illuminate\Http\Request;


/*
    Trait
    ----------
    All Trait nessecary for this Pruchase Controller
*/
use App\Http\Traits\ProductsTrait;
use App\Http\Traits\UsersTrait;
use App\Http\Traits\POS_Trait;
// use App\Htpp\Traits\POS_Trait;

/*
    Auth 
    ------
    Call Auth To get User Auth Identity that LOGIN now
*/

use Auth;

class PurchaseController extends Controller
{
    //Call Trait
    use ProductsTrait;
    use UsersTrait;
    use POS_Trait;
    // use POS_Trait;

	public function __construct(Products $products,
								Temp_Customer $temp_customer,
								Temp_Purchase $temp_purchase,
								Temp_Purchase_Detail $temp_purchase_detail,
								Purchase $purchase,
								Purchase_Detail $purchase_detail,
								User $user)
	{
		$this->middleware('auth');
		$this->products = $products;
		$this->user = $user;
    	$this->temp_customer = $temp_customer;
    	$this->temp_purchase = $temp_purchase;
    	$this->temp_purchase_detail = $temp_purchase_detail;
    	$this->purchase = $purchase;
    	$this->purchase_detail = $purchase_detail;
    }


	public function index()
	{
    	$products_list = $this->showProductsNotZero();

    	return view('static/pos/sales',[
    		'products_list'=>$products_list,
    		'temp_purchase_detail'=>[],
    		'subtotal'=>0
    	]);
    }

        public function addProductCart(Request $request)
        {   
            $staff_id = Auth::ID();
        
            /*
                Get User ID From NIM
                Using Trait To Get USER ID;
                
            */

            $nim = $request->input('nim');
            $customer_id = $this->GetUserID($nim);

            $type= $request->input('type');

            /*
                Using ProductsTrait To Call Find Product Function
                Search Text = Send By Jquery Data (Barcode / Products ID)
                Type = What coloumn will we use 4 Search the Product . (Barcode / Product ID)
                Return in Array 
                Get Product ID & Price;

            */

            if($type=="barcode")
            {
                $search_text = $request->input('barcode');

                $return_search = $this->findProduct($search_text,$type);

                $price = $return_search[0]['price'];             
                $product_id = $return_search[0]['id'];
                $stock = $return_search[0]['stock'];                
            }
            else if($type == "product_id")
            {
                $search_text = $request->input('product_id');
                $return_search = $this->findProduct($search_text,$type);

                $price = $return_search['price'];             
                $product_id = $search_text;
                $stock = $return_search['stock'];
            }

            /*
                Check is the Customer Has Purchase That has not been done or No
                IF No , We Will Create New Temp Pruchase * Purchase Detail. 
                If Yes, we Will take the ID TEMP Purchase 4 getting Data From Temp Detail 
            */
                $check_purchase = $this->FindTempPurchaseFromCustomerID($customer_id);

                if($check_purchase->count()==0)
                {
                    $arr_temp_purchase = [
                        'customer_id'=>$customer_id,
                        'staff_id'=>$staff_id
                    ];

                    $createTempPurchase = $this->CreateTempPurchase($arr_temp_purchase);
                    $temp_purchase_id = $createTempPurchase->id;   

                    $arr_temp_purchase_detail = [
                        'qty'=>1,
                        'product_id'=>$product_id,
                        'price'=>$price,
                        'temp_purchase_id'=>$temp_purchase_id
                    ];  

                    $this->CreateTempPurchaseDetail($arr_temp_purchase_detail);

                    $temp_purchase_detail = $this->selectTempPurchaseDetailFromTempID($temp_purchase_id)->get();
                    return $this->returnViewPurchaseDetail($temp_purchase_detail);

                }else{

                    $temp_purchase_id = $check_purchase->value('id');   

                }
                
                /*
                    Check IF The Product Has Been Added To The Temp Purchase Detail b4 or not 
                    
                    If The Product Has been Added ,Check is The Product Stock Is Enough Or Not Before Adding More To The Cart
                    If Not Enough , It Will Return Failed . If Enough , it will update the temp purchase
                */
                $CheckTempDetail = $this->CheckTempDetail($product_id,$temp_purchase_id);

                $qty = $CheckTempDetail->value('qty')+1;

                // From POS Trait Check Is Stock Enough Or NOt
                $stock_status = $this->CheckStockEnough($stock,$qty);

                if($CheckTempDetail->count()==0){
                    $arr_temp_purchase_detail = [
                        'qty'=>1,
                        'product_id'=>$product_id,
                        'price'=>$price,
                        'temp_purchase_id'=>$temp_purchase_id
                    ];  
                    $this->CreateTempPurchaseDetail($arr_temp_purchase_detail);
                }else{

                    if($stock_status==0){
                        $returnArr = [
                            'status' => 'failed',
                            'message' => 'Stok Tidak Cukup !!'
                        ];
                 
                        return response()->json($returnArr);
                    }else{
                        $temp_purchase_detail_id = $CheckTempDetail->value('id');
                        $qty_product_temp_purchase_detail = $CheckTempDetail->value('qty');
                        $qty = $qty_product_temp_purchase_detail+1;
                        $arr_temp_purchase_detail = [
                            'qty'=>$qty,
                        ];  
                        
                          $this->UpdateTempPurchaseDetail($temp_purchase_detail_id,$arr_temp_purchase_detail);
                    }

                }
                
                $temp_purchase_detail = $this->selectTempPurchaseDetailFromTempID($temp_purchase_id)->get();
                return $this->returnViewPurchaseDetail($temp_purchase_detail);
        }    

        public function plusMinusItem($id,$status)
        {
            /*
                $id = Temp Purchase Detail ID
                $status = plus/minus/delete
            */
            $return_temp_purchase_detail = $this->SelectTempPurchaseDetail($id);

            $temp_purchase_id = $return_temp_purchase_detail['temp_purchase_id'];
            $temp_product_id = $return_temp_purchase_detail['product_id'];

            /*
                Get Product Detail From Temp Product ID To Take The Stock
            */
            $return_search = $this->findProduct($temp_product_id,'product_id');
            $stock = $return_search['stock'];

            $qty =  $return_temp_purchase_detail['qty'];
            if($status=='plus'){
                $qty = $qty+1;
            }else if($status=="minus"){
                $qty = $qty-1;
            }else if($status=="delete"){
                $qty = 0;
            }
                // From POS Trait Check Is Stock Enough Or NOt
            $stock_status = $this->CheckStockEnough($stock,$qty);
            if($stock_status==0){
                $returnArr = [
                    'status' => 'failed',
                    'message' => 'Stok Tidak Cukup !!'
                ];
         
                return response()->json($returnArr);
            }else{
                $arr_temp_purchase_detail = [
                    'qty'=>$qty
                ];
                    if($qty==0){
                        $return_temp_purchase_detail->delete();   
                    }else{
                        $return_temp_purchase_detail->update($arr_temp_purchase_detail);
                    }

                $temp_purchase_detail = $this->selectTempPurchaseDetailFromTempID($temp_purchase_id)->get();
                return $this->returnViewPurchaseDetail($temp_purchase_detail);
            }

            

        }

        public function refreshCart(Request $request)
        {
            $nim = $request->input('nim');
            $customer_id = $this->GetUserID($nim);

            $temp_purchase_id = $this->FindTempPurchaseFromCustomerID($customer_id)->value('id');

            $temp_purchase_detail = $this->selectTempPurchaseDetailFromTempID($temp_purchase_id)->get();
            return $this->returnViewPurchaseDetail($temp_purchase_detail);
        }

        public function clearCart(Request $request)
        {
                $temp_purchase_id = $request->input('temp_purchase_id');

                $this->FindTempPurchase($temp_purchase_id)->delete();
                $this->selectTempPurchaseDetailFromTempID($temp_purchase_id)->delete();

                $temp_purchase_detail = $this->selectTempPurchaseDetailFromTempID($temp_purchase_id)->get();

                return $this->returnViewPurchaseDetail($temp_purchase_detail);
        }

        
        public function proses($data)
        {   
            $staff_id = Auth::ID();

            $data = explode('|',$data);
            $nim = $data[0];

            $customer_id = $this->GetUserID($nim);
            $temp_purchase_id = $this->FindTempPurchaseFromCustomerID($customer_id)->value('id');

            $arr_purchase = [
                'customer_id'=>$customer_id,
                'staff_id'=>$staff_id,
                'disc'=>$data[1],
                'tax'=>$data[2],
                'subtotal'=>0,
                'gtotal'=>0
            ];

            $subtotal = 0;

            $create_purchase = $this->CreatePurchase($arr_purchase);
            $purchase_id = $create_purchase->id;

            $temp_purchase_detail = $this->selectTempPurchaseDetailFromTempID($temp_purchase_id)->get();

            //Move Data From Temp Purchase Detail To Purchase Detail

            foreach ($temp_purchase_detail as $data_purchase_detail){
                $arr_purchase_detail =[
                    'purchase_id'=>$purchase_id,
                    'product_id'=>$data_purchase_detail['product_id'],
                    'qty'=>$data_purchase_detail['qty'],
                    'price'=>$data_purchase_detail['price'],
                    'total'=>$data_purchase_detail['qty']*$data_purchase_detail['price'],
                ];

                $subtotal += $data_purchase_detail['qty']*$data_purchase_detail['price'];

                $this->purchase_detail->create($arr_purchase_detail);    

                //Change Product Stock In Master Table

                $product_id = $data_purchase_detail['product_id'];

                $return_search = $this->findProduct($product_id,'product_id');

                $stock_before = $return_search['stock'];
                $stock_after =  $stock_before-$data_purchase_detail['qty'];

                $arr_product = [
                    'stock'=> $stock_after
                ];

                $this->updateProduct($product_id,$arr_product);

            }
            
            $gtotal = $subtotal-$data[1];
            $arr_purchase = [
                'subtotal'=>$subtotal,
                'gtotal'=>$gtotal
            ];

            $this->UpdatePurchase($purchase_id,$arr_purchase);

            $this->selectTempPurchaseDetailFromTempID($temp_purchase_id)->delete();
            $this->FindTempPurchase($temp_purchase_id)->delete();

            $purchase_detail =$this->SelectPurchaseDetail($purchase_id)->get();

            return view('static/pos/salesReport',[
                'data_purchase_detail'=>$purchase_detail
            ]);

    }

    public function returnViewPurchaseDetail($temp_purchase_detail){
        return view('/static/pos/PurchaseDetail',[
            'temp_purchase_detail'=>$temp_purchase_detail,
            'subtotal'=>0
        ]); 
    }

    /*
        Sales History Ko Anthony
    */

    public function salesHistory(){
        $startDate = request('startDate');
        $endDate = request('endDate');
        if( isset( $startDate,$endDate ) && !empty( $startDate ) && !empty( $endDate ) ){
            $sales = \App\Http\Models\Purchase::with(['purchaseDetail','staff','customer'])
            ->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'")
            ->orderBy('created_at','desc')
            ->get();
        }
        else{
            $sales = \App\Http\Models\Purchase::with(['purchaseDetail','staff','customer'])
            ->orderBy('created_at','desc')
            ->get();
        }

        
        /*$sales = \App\Http\Models\Purchase::all();*/
        /*foreach ($sales as $sale) {
            echo '<pre>',print_r($sale),'<pre>';
            foreach ($sale->purchaseDetail as $purchaseDetailEach) {
                echo '<pre>',print_r($purchaseDetailEach),'<pre>';
            }
            var_dump($sale->purchaseDetail) ;
            echo '<pre>',print_r($sale->purchaseDetail),'<pre>';
            foreach ($sale->purchaseDetail as $purchaseDetailEach) {
                 echo $purchaseDetailEach->product->product_name . '<br>';
            }
           
        }*/
        $periodOption = [];
        $periodOption['currentDay'] = ['startDate' => date('Y-m-d'), 'endDate' => date('Y-m-d')];
        $periodOption['currentMonth'] = ['startDate' => date('Y-m-01'), 'endDate' => date('Y-m-t')];
        $periodOption['yesterday'] = ['startDate' => date('Y-m-d',strtotime('yesterday')), 'endDate' => date('Y-m-d',strtotime('yesterday')) ];
        $periodOption['last7Days'] = ['startDate' => date('Y-m-d',strtotime('-7 days')), 'endDate' => date('Y-m-d') ];
        $periodOption['last30Days'] = ['startDate' => date('Y-m-d',strtotime('-30 days')), 'endDate' => date('Y-m-d') ];

        return view('static.pos.salesHistory',compact('sales','periodOption'));
        /*dd($sales);*/

    }

    public function salesReport(){
        $timeUnit = [];
        $startDate = request('startDate');
        $endDate = request('endDate');

        $purchase = \App\Http\Models\Purchase::with(['purchaseDetail'=>function($query){$query->groupBy('product_id');}])->get();

        $soldProductArr = [];
        $soldCategoryArr = [];

        /*$startDate = request('startDate');
        $endDate = request('endDate');
        if( isset( $startDate,$endDate ) && !empty( $startDate ) && !empty( $endDate ) ){

            $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod()
            ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
            ->get();
            $sumPerCategory = \App\Http\Models\Products::sumPerCategory()
            ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
            ->get();
            $sumPerProduct = \App\Http\Models\Products::sumPerProduct()
            ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
            ->get();
        }
        else{
            $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod()->get();
            $sumPerCategory = \App\Http\Models\Products::sumPerCategory()->get();
            $sumPerProduct = \App\Http\Models\Products::sumPerProduct()->get();
        }*/

        $timeUnitOption = request('timeUnitOption');
        if( isset( $timeUnitOption ) && !empty( $timeUnitOption )){
           if($timeUnitOption == 'Year'){
                $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod('DATE_FORMAT(purchase.created_at,"%Y")')->get();
                $sumPerCategory = \App\Http\Models\Products::sumPerCategory('DATE_FORMAT(purchase.created_at,"%Y")')->get();
                $sumPerProduct = \App\Http\Models\Products::sumPerProduct('DATE_FORMAT(purchase.created_at,"%Y")')->get();

                $timeUnit['unit'] = 'year';
           }
           else if($timeUnitOption == 'Month'){
                if( isset( $startDate,$endDate ) && !empty( $startDate ) && !empty( $endDate ) ){

                    $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod()
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();
                    $sumPerCategory = \App\Http\Models\Products::sumPerCategory()
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();
                    $sumPerProduct = \App\Http\Models\Products::sumPerProduct()
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();


                    $purchase = \App\Http\Models\Purchase::with(['purchaseDetail'=>function($query){$query->groupBy('product_id');}])->whereRaw("DATE_FORMAT(created_at,'%Y-%m') BETWEEN '$startDate' AND '$endDate'")->get();

                    $sumDiscPerPeriod = \App\Http\Models\Purchase::sumDiscPerPeriod()
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();
                }
                else{
                    $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod()->get();
                    $sumPerCategory = \App\Http\Models\Products::sumPerCategory()->get();
                    $sumPerProduct = \App\Http\Models\Products::sumPerProduct()->get();

                    $sumDiscPerPeriod = \App\Http\Models\Purchase::sumDiscPerPeriod()->get();
                }

                

                $timeUnit['unit'] = 'month';
           }
           else if($timeUnitOption == 'Day'){

                if( isset( $startDate,$endDate ) && !empty( $startDate ) && !empty( $endDate ) ){

                    $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();
                    $sumPerCategory = \App\Http\Models\Products::sumPerCategory('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();
                    $sumPerProduct = \App\Http\Models\Products::sumPerProduct('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();

                    $purchase = \App\Http\Models\Purchase::with(['purchaseDetail'=>function($query){$query->groupBy('product_id');}])->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'")->get();

                    $sumDiscPerPeriod = \App\Http\Models\Purchase::sumDiscPerPeriod('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();
                }
                else{
                    $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')->get();
                    $sumPerCategory = \App\Http\Models\Products::sumPerCategory('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')->get();
                    $sumPerProduct = \App\Http\Models\Products::sumPerProduct('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')->get();

                    $sumDiscPerPeriod = \App\Http\Models\Purchase::sumDiscPerPeriod('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')->get();
                }


                

                $timeUnit['unit'] = 'day';
           } 
        }
        else{
            $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod()->get();
            $sumPerCategory = \App\Http\Models\Products::sumPerCategory()->get();
            $sumPerProduct = \App\Http\Models\Products::sumPerProduct()->get();

            $sumDiscPerPeriod = \App\Http\Models\Purchase::sumDiscPerPeriod()->get();

            $timeUnit['unit'] = 'month';
        }
        


        

        /*$category = \App\Http\Models\Category::all();*/

        foreach($purchase as $purchaseEach){
            foreach($purchaseEach->purchaseDetail as $purchaseDetailEach){
                $soldProductArr[] = $purchaseDetailEach->product;
                $soldCategoryArr[] = $purchaseDetailEach->product->category;
            }
            
        }

        $soldCategoryArr = array_unique($soldCategoryArr);

        $soldCategoryArr = collect($soldCategoryArr);
        $soldProductArr = collect($soldProductArr);

        /*foreach ($sumPerPeriod as $sumPerPeriodEach) {
            $allMonths[] = ['month'=>date('F',strtotime($sumPerPeriodEach->time_unit)),'year'=>date('Y',strtotime($sumPerPeriodEach->time_unit))];
        }
*/
        foreach ($sumPerPeriod as $sumPerPeriodEach) {
            $timeUnit[] = $sumPerPeriodEach->time_unit;
        }

        $periodOption = [];
        $periodOption['currentDay'] = ['startDate' => date('Y-m-d'), 'endDate' => date('Y-m-d')];
        $periodOption['currentMonth'] = ['startDate' => date('Y-m-01'), 'endDate' => date('Y-m-t')];
        $periodOption['yesterday'] = ['startDate' => date('Y-m-d',strtotime('yesterday')), 'endDate' => date('Y-m-d',strtotime('yesterday')) ];
        $periodOption['last7Days'] = ['startDate' => date('Y-m-d',strtotime('-7 days')), 'endDate' => date('Y-m-d') ];
        $periodOption['last30Days'] = ['startDate' => date('Y-m-d',strtotime('-30 days')), 'endDate' => date('Y-m-d') ];

        
        /*echo "<pre>",print_r($allMonths),"<pre>";*/

        return view('static.pos.salesReportAnthony',compact('sumPerPeriod','sumPerCategory','sumPerProduct','timeUnit','soldProductArr','soldCategoryArr','periodOption','sumDiscPerPeriod'));
    } 
}
