<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\News;

class NewsController extends Controller
{
    //
    public function index(){
    	$news = News::all();
    	return view('static.news.index',compact('news'));
    }

    public function show(News $news){
    	return view('static.news.show',compact('news'));
    }

    public function store(){
    	$this->validate(request(),[
    	        'subject' => 'required|min:4',
    	        'content' => 'required|min:10'
    	    ]);
    	News::create([
    			'subject' => request('subject'),
    			'content' => request('content')
    		]);
    	return redirect('/news');
    }



    public function destroy(News $news){
    	$news->delete();
    	return redirect('/news');
    }

    public function update(News $news){
    	$this->validate(request(),[
    	        'subject' => 'required|min:4',
    	        'content' => 'required|min:10'
    	    ]);

    	$news->update([
    			'subject' => request('subject'),
    			'content' => request('content')
    		]);
    	return redirect('/news');
    }
}
