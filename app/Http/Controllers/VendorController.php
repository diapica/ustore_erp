<?php

namespace App\Http\Controllers;

/*
 * Models
 * ---
 * 
 * All models nessecary for this controller
 * 
 */
use App\Http\Models\Vendor;

/*
 * Modules Dependencies
 * ---
 * 
 * All functional dependencies for this controller
 * 
 */

use Illuminate\Http\Request;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Vendor::all()->toArray();

        return view('static.vendor.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('static.vendor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = $this->validate(request(), [
            'barcode' => 'required|numeric',
            'name' => 'required',
            'stock' => 'required|numeric',
            'price' => 'required|numeric'
        ]);

        Vendor::create($product);

        return back()->with('success', 'Product has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Vendor::find($id);

        return view('static.vendor.edit', compact('product','id'));
    }

    public function update(Request $request, $id)
    {
        $product = Vendor::find($id);
        $this->validate(request(), [
            'barcode' => 'required|numeric',
            'name' => 'required',
            'stock' => 'required|numeric',
            'price' => 'required|numeric'
        ]);

        $product->barcode = $request->get('barcode');
        $product->name = $request->get('name');
        $product->stock = $request->get('stock');
        $product->price = $request->get('price');

        $product->save();

        return redirect('/vendor')->with('success','Product has been updated');
    }

    public function destroy($id)
    {
        $product = Vendor::find($id)->delete();
        return redirect('/vendor')->with('success','Product has been deleted');
    }
}
