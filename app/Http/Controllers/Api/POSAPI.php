<?php

namespace App\Http\Controllers\Api;

/*
 * Models
 * ---
 * 
 * All models nessecary for this controller
 * 
 */


/*
 * Modules Dependencies
 * ---
 * 
 * All functional dependencies for this controller
 * 
 */
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/*
	Trait
	----------
	All Trait nessecary for this Api Controller
*/
use App\Http\Traits\ProductsTrait;


class POSAPI extends Controller
{
	use ProductsTrait;

    public function POSfindProduct(Request $request)
    {
    	$search_text = $request->input('search');

        $type= $request->input('type');

        /*
        	Search Text = Item That Will We Search in Products . Got From Barcode / Search Text Box
        	Type = What coloumn will we use 4 Search the Product . (Product Name & Barcode)
        */
        	
        $return_search = $this->findProduct($search_text,$type);

    	return view('/static/pos/SearchProducts',[
    		'products_list'=>$return_search
    	]);

    }
	
    /*
	 * addCustomer(Request $request)
	 * ---
	 * 
	 * @request:
	 * 
	 */
	public function addCustomer(Request $request)
	{	
		$name = $request->input('name');
		$nim = $request->input('nim');
		$email = $request->input('email');
		if ($this->user->findUser($nim, $email) != 0)
		{
			return ['reg_status' => 'USER_EXIST'];
		}
		
		$this->user->create([
			'name' => $name,
			'email' => $email,
			'nim' => $nim,
			'fakultas' => $request->input('fakultas'),
			'jurusan' => $request->input('jurusan'),
			'angkatan' => $request->input('angkatan'),
			'password'=> bcrypt('umnstore')
		]);
  }
}
