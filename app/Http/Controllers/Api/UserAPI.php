<?php

namespace App\Http\Controllers\Api;

/*
 * Models
 * ---
 * 
 * All models nessecary for this controller
 * 
 */
Use App\Http\Models\User;

/*
 * Modules Dependencies
 * ---
 * 
 * All functional dependencies for this controller
 * 
 */
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserAPI extends Controller
{

	public function __construct(User $user){
		//$this->middleware('auth');
		$this->user = $user;
    }

    /*
	 * findCustomer(Request $request)
	 * ---
	 * 
	 * @request:
	 * 
	 */
    public function findCustomer(Request $request)
    {
        $nim = $request->input('nim');
        return $this->user->findUser($nim);
    }
	
    /*
	 * addCustomer(Request $request)
	 * ---
	 * 
	 * @request:
	 * 
	 */
	public function addCustomer(Request $request)
	{	
		$name = $request->input('name');
		$nim = $request->input('nim');
		$email = $request->input('email');
		if ($this->user->findUser($nim, $email) != 0)
		{
			return ['reg_status' => 'USER_EXIST'];
		}
		
		$this->user->create([
			'name' => $name,
			'email' => $email,
			'nim' => $nim,
			'fakultas' => $request->input('fakultas'),
			'jurusan' => $request->input('jurusan'),
			'angkatan' => $request->input('angkatan'),
			'password'=> bcrypt('umnstore')
		]);
  }
}
