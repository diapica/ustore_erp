<?php

namespace App\Http\Controllers;

/*
 * Models
 * ---
 * 
 * All models nessecary for this controller
 * 
 */
use App\Http\Models\Products;
use App\Http\Models\Category;
use App\Http\Models\Stock_History;

/*
 * Modules Dependencies
 * ---
 * 
 * All functional dependencies for this controller
 * 
 */
use Illuminate\Http\Request;

use Auth;

class ProductsController extends Controller
{

	public function __construct(Products $products,Category $category,Stock_History $stock_history)
	{
		$this->middleware('auth');
		$this->products = $products;
        $this->category = $category;
        $this->stock_history = $stock_history;
		$company_code = 57257497;
    	$mul_sum_comp_code = (5*1)+(7*3)+(2*1)+(5*3)+(7*1)+(4*3)+(9*1)+(7*3);
    	$this->mul_sum_comp_code = $mul_sum_comp_code;
    	$this->company_code = $company_code;
	}

    public function index(){
        $products = $this->products->paginate(10);
        $categories = $this->category->all();
        return view('static/warehouse/ProductsList',[
            'products'=>$products,
            'categories'=>$categories
        ]);
        
    }

    public function save(request $request){
        // $this->validate(request(), [
     //        'product_name' => 'required',
     //        'stock' => 'required|numeric',
     //        'price' => 'required|numeric',
     //        'status'=> 'required!numeric',
     //        'vendor' => 'required!numeric',
     //        'category' => 'required!numeric',
     //    ]);
        $staff_id = Auth::ID();
        $product = [
            'product_name' =>  $request->input('product_name'),
            'stock' => $request->input('stock'),
            'price' => $request->input('price'),
            'status' =>  $request->input('status'),
            'vendor_id' => $request->input('vendor'),
            'desc' => $request->input('desc'),
            'category_id' => $request->input('category'),
            'image'=>"",
        ];

        $cmd = $request->input('cmd');

        if($cmd=="Add Product"){
    		$product_id = $this->products->create($product)->id;
	        $id_product_ = sprintf("%04d",$product_id);        
	        $id_product = str_split($id_product_);
	        $mul_sum_id_prod = ($id_product[0]*1)+($id_product[1]*3)+($id_product[2]*1)+($id_product[3]*3);
	        $mul_sum_all = $this->mul_sum_comp_code+$mul_sum_id_prod;
	        $bil = str_split($mul_sum_all);
	        $jumlah_bil = count($bil);
		        if($bil[$jumlah_bil-1]<5){
		        	 if($jumlah_bil==2){
		        	 	$verif =  round($mul_sum_all,-1)-$mul_sum_all;	
		        	 	
		        	 }else{
		        	 	$total =  $mul_sum_all-round($mul_sum_all,-1);	
		        	 	if($total!=0){
		        	 		$verif = 10-$total;
		        	 	}else{
		        	 		$verif = $total;
		        	 	}
		        	 }
		        }else{
		        	 if($jumlah_bil==2){
		        			$verif =  round($mul_sum_all,-1)-$mul_sum_all;
		        	 }else{
		        	 		$verif =  $mul_sum_all-round($mul_sum_all,-1);
		        	 }
		        }
	       
	        

	      	$verif = abs($verif);
	      	$barcode = $this->company_code.$id_product_.$verif;

	      	$params = [
	      		'barcode'=>$barcode,
	      	];

	      	$this->products->find($product_id)->update($params);
	      	return redirect('/products');
    	}else{
    		$product_id = $request->input('sembunyi');
    		$product_update = $this->products->find($product_id);
            $stock_before = $product_update['stock'];
            $stock_after = $request->input('stock');
            if($stock_before!=$stock_after){
                $params_stock_history = [
                    'before'=>$stock_before,
                    'after'=>$stock_after,
                    'product_id'=>$product_id,
                    'user_id'=>$staff_id
                ];
                $this->stock_history->create($params_stock_history);
            }

            $product_update->update($product);
    		return redirect()->back();
    	}

        
       	
    	
    }

    public function productDetail($id,$name){
    	$product_detail = $this->products->find($id);
        $categories = $this->category->all();
        $stock_history = $this->stock_history->where('product_id','=',$id)->get();
    	return view('static/warehouse/ProductsDetail',[
    		'product_name'=>$name,
    		'product_detail' => $product_detail,
            'categories'=>$categories,
            'stocks_history' => $stock_history
    	]);
    }

    public function deleteProduct($id){
    	$this->products->find($id)->delete();
    	return redirect('/products');
    }

    public function printBarcode($id){
        if($id=='all'){
            $products = $this->products->all();
             return view("/static/warehouse/PrintAllBarcode",[
                'products'=>$products,
            ]);
        }else{                
            $product = $this->products->find($id);
            $barcode = $product['barcode'];
            $product_name = $product['product_name'];

            return view("/static/warehouse/PrintBarcode",[
                'barcode'=>$barcode,
                'product_name'=>$product_name
            ]);
        }
    }



}

