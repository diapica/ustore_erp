<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class WarehouseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $group_id = Auth::user()->id_group;
        if($group_id != 12 && $group_id != 4 && $group_id != 5 && $group_id != 6 && $group_id != 7 && $group_id != 8){
            return redirect("/");
        }

        return $next($request);
    }
}
