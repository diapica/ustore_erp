<?php

namespace App\Http\Controllers;
/*
 * Models
 * ---
 * 
 * All models nessecary for this controller
 * 
 */
use App\Http\Models\Products;
Use App\Http\Models\User;
Use App\Http\Models\Temp_Customer;
use App\Http\Models\Temp_Purchase;
use App\Http\Models\Temp_Purchase_Detail;
use App\Http\Models\Purchase;
use App\Http\Models\Purchase_Detail;

/*
 * Modules Dependencies
 * ---
 * 
 * All functional dependencies for this controller
 * 
 */
use Illuminate\Http\Request;


class BackupPurchaseController extends Controller
{
    public function __construct(Products $products,
                                Temp_Customer $temp_customer,
                                Temp_Purchase $temp_purchase,
                                Temp_Purchase_Detail $temp_purchase_detail,
                                Purchase $purchase,
                                Purchase_Detail $purchase_detail,
                                User $user)
    {
        $this->middleware('auth');
        $this->products = $products;
        $this->user = $user;
        $this->temp_customer = $temp_customer;
        $this->temp_purchase = $temp_purchase;
        $this->temp_purchase_detail = $temp_purchase_detail;
        $this->purchase = $purchase;
        $this->purchase_detail = $purchase_detail;
    }

    public function index()
    {
        $products_list = $this->products->where('stock',"!=",'0')->get();
        return view('static/pos/sales',[
            'products_list'=>$products_list,
            'temp_purchase_detail'=>[],
            'subtotal'=>0
        ]);
    }

    public function search(Request $request){
        $search_name = $request->input('cari');
        $type= $request->input('type');
        if($type=="name"){
           $products_list = $this->products->where('product_name','like','%'.$search_name.'%')->where('stock',"!=",'0')->get();
        }else{
            $products_list = $this->products->where('barcode','=',$search_name)->where('stock',"!=",'0')->get();
        }
        return view('/static/pos/SearchProducts',[
            'products_list'=>$products_list
        ]);
    }

    public function addProductCart(Request $request){
        
        $nim = $request->input('nim');
        $qty = '1';
        $type= $request->input('type');

        if($type=="barcode"){
            $barcode = $request->input('barcode');
            $product = $this->products->where('barcode','=',$barcode)->get();
            $price = $product[0]['price'];             
            $product_id = $product[0]['id'];
            $stock = $Product[0]['stock'];             
        }else{
            $product_id = $request->input('product_id');
            $product = $this->products->find($product_id);
            $price = $product['price'];   
            $stock = $Product['stock'];
        }

        
        $check_pruchase = $this->temp_purchase->where('customer_id','=',$nim);
        if($check_pruchase->count()==0){
            $arr_temp_purchase = [
                'customer_id'=>$nim,
                'staff_id'=>'0' //0 = Admin
            ];
            $id_temp_purchase = $this->temp_purchase->create($arr_temp_purchase)->id;   
        }else{
            $id_temp_purchase = $check_pruchase->value('id');   
            

        }
        $product_stock = $this->products->find($product_id);

        $check_product = $this->temp_purchase_detail->where('product_id','=',$product_id)->where('temp_purchase_id','=',$id_temp_purchase);
        if($check_product->count()==0){
            $arr_temp_purchase_detail = [
                'qty'=>$qty,
                'product_id'=>$product_id,
                'price'=>$price,
                'temp_purchase_id'=>$id_temp_purchase
            ];  
            $this->temp_purchase_detail->create($arr_temp_purchase_detail);
        }else{

            if($product_stock['stock'] < $check_product->value('qty')+$qty ){
                $returnArr = [
                    'status' => 'failed',
                    'message' => 'Stok Tidak Cukup !!'
                ];
         
                return response()->json($returnArr);
            }else{
                $id_temp_purchase_detail = $check_product->value('id');
                $qty_product_temp_purchase_detail = $check_product->value('qty');
                $qty = $qty_product_temp_purchase_detail+$qty;
                $arr_temp_purchase_detail = [
                    'qty'=>$qty,
                ];  
                
                  $this->temp_purchase_detail->find($id_temp_purchase_detail)->update($arr_temp_purchase_detail);
            }

        }
        
        $temp_purchase_detail = $this->temp_purchase_detail->where('temp_purchase_id','=',$id_temp_purchase)->get();
        return view('/static/pos/PurchaseDetail',[
            'temp_purchase_detail'=>$temp_purchase_detail,
            'subtotal'=>0
        ]);
    }

    public function plusMinusItem($id,$status){
        $temp_product = $this->temp_purchase_detail->find($id);

        $id_temp_purchase = $temp_product['temp_purchase_id'];

        $qty =  $temp_product['qty'];
        if($status=='plus'){
            $qty = $qty+1;
        }else if($status=="minus"){
            $qty = $qty-1;
        }else if($status=="delete"){

        }
        
        $arr = [
            'qty'=>$qty
        ];

        if($status=='plus' || $status=='minus'){    
            if($qty==0){
                $this->temp_purchase_detail->find($id)->delete();   
            }else{
                $this->temp_purchase_detail->find($id)->update($arr);
            }
        }else{
            $this->temp_purchase_detail->find($id)->delete();
        }

        $temp_purchase_detail = $this->temp_purchase_detail->where('temp_purchase_id','=',$id_temp_purchase)->get();
        return view('/static/pos/PurchaseDetail',[
            'temp_purchase_detail'=>$temp_purchase_detail,
            'subtotal'=>0
        ]);     
    }

    public function refreshCart(Request $request){
        $nim = $request->input('nim');
        $id_temp_purchase = $this->temp_purchase->where('customer_id','=',$nim)->value('id');
        $temp_purchase_detail = $this->temp_purchase_detail->where('temp_purchase_id','=',$id_temp_purchase)->get();
        return view('/static/pos/PurchaseDetail',[
            'temp_purchase_detail'=>$temp_purchase_detail,
            'subtotal'=>0
        ]);     
    }

    public function clearCart(Request $request){
        $temp_purchase_id = $request->input('temp_purchase_id');
        $this->temp_purchase->find($temp_purchase_id)->delete();
        $this->temp_purchase_detail->where('temp_purchase_id','=',$temp_purchase_id)->delete();
        return view('/static/pos/PurchaseDetail',[
            'temp_purchase_detail'=>[],
            'subtotal'=>0
        ]); 

    }

    public function proses($data){
        $data = explode('|',$data);
        $temp_purchase_id = $this->temp_purchase->where('customer_id','=',$data[0])->value('id');
        $arr_purchase = [
            'customer_id'=>$data[0],
            'staff_id'=>0,
            'disc'=>$data[1],
            'tax'=>$data[2],
            'subtotal'=>0,
            'gtotal'=>0
        ];

        $subtotal = 0;

        $id_purchase = $this->purchase->create($arr_purchase)->id;

        $temp_purchase_detail = $this->temp_purchase_detail->where('temp_purchase_id','=',$temp_purchase_id)->get();
        foreach ($temp_purchase_detail as $data_purchase_detail){
            $arr_purchase_detail =[
                'purchase_id'=>$id_purchase,
                'product_id'=>$data_purchase_detail['product_id'],
                'qty'=>$data_purchase_detail['qty'],
                'price'=>$data_purchase_detail['price'],
                'total'=>$data_purchase_detail['qty']*$data_purchase_detail['price'],
            ];

            $subtotal += $data_purchase_detail['qty']*$data_purchase_detail['price'];

            $qty_product = $this->products->where('id','=',$data_purchase_detail['product_id'])->value('stock');
            $arr_product = [
                'stock'=> $qty_product-$data_purchase_detail['qty'],
            ];

            $this->products->find($data_purchase_detail['product_id'])->update($arr_product);

            $this->purchase_detail->create($arr_purchase_detail);
        };
        
        $gtotal = $subtotal-$data[1];
        $arr_purchase_update = [
            'subtotal'=>$subtotal,
            'gtotal'=>$gtotal
        ];
        $this->purchase->find($id_purchase)->update($arr_purchase_update);

        $this->temp_purchase->find($temp_purchase_id)->delete();
        $this->temp_purchase_detail->where('temp_purchase_id','=',$temp_purchase_id)->delete();

        $purchase_detail =$this->purchase_detail->where('purchase_id','=',$id_purchase)->get();

        return view('static/pos/salesReport',[
            'data_purchase_detail'=>$purchase_detail
        ]);

    }

    public function salesHistory(){
        $startDate = request('startDate');
        $endDate = request('endDate');
        if( isset( $startDate,$endDate ) && !empty( $startDate ) && !empty( $endDate ) ){
            $sales = \App\Http\Models\Purchase::with(['purchaseDetail','staff','customer'])
            ->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'")
            ->orderBy('created_at','desc')
            ->get();
        }
        else{
            $sales = \App\Http\Models\Purchase::with(['purchaseDetail','staff','customer'])
            ->orderBy('created_at','desc')
            ->get();
        }

        
        /*$sales = \App\Http\Models\Purchase::all();*/
        /*foreach ($sales as $sale) {
            echo '<pre>',print_r($sale),'<pre>';
            foreach ($sale->purchaseDetail as $purchaseDetailEach) {
                echo '<pre>',print_r($purchaseDetailEach),'<pre>';
            }
            var_dump($sale->purchaseDetail) ;
            echo '<pre>',print_r($sale->purchaseDetail),'<pre>';
            foreach ($sale->purchaseDetail as $purchaseDetailEach) {
                 echo $purchaseDetailEach->product->product_name . '<br>';
            }
           
        }*/
        $periodOption = [];
        $periodOption['currentDay'] = ['startDate' => date('Y-m-d'), 'endDate' => date('Y-m-d')];
        $periodOption['currentMonth'] = ['startDate' => date('Y-m-01'), 'endDate' => date('Y-m-t')];
        $periodOption['yesterday'] = ['startDate' => date('Y-m-d',strtotime('yesterday')), 'endDate' => date('Y-m-d',strtotime('yesterday')) ];
        $periodOption['last7Days'] = ['startDate' => date('Y-m-d',strtotime('-7 days')), 'endDate' => date('Y-m-d') ];
        $periodOption['last30Days'] = ['startDate' => date('Y-m-d',strtotime('-30 days')), 'endDate' => date('Y-m-d') ];

        return view('static.pos.salesHistoryAnthony',compact('sales','periodOption'));
        /*dd($sales);*/

    }

    public function salesReport(){
        $timeUnit = [];
        $startDate = request('startDate');
        $endDate = request('endDate');
        /*$startDate = request('startDate');
        $endDate = request('endDate');
        if( isset( $startDate,$endDate ) && !empty( $startDate ) && !empty( $endDate ) ){

            $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod()
            ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
            ->get();
            $sumPerCategory = \App\Http\Models\Products::sumPerCategory()
            ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
            ->get();
            $sumPerProduct = \App\Http\Models\Products::sumPerProduct()
            ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
            ->get();
        }
        else{
            $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod()->get();
            $sumPerCategory = \App\Http\Models\Products::sumPerCategory()->get();
            $sumPerProduct = \App\Http\Models\Products::sumPerProduct()->get();
        }*/

        $timeUnitOption = request('timeUnitOption');
        if( isset( $timeUnitOption ) && !empty( $timeUnitOption )){
           if($timeUnitOption == 'Year'){
                $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod('DATE_FORMAT(purchase.created_at,"%Y")')->get();
                $sumPerCategory = \App\Http\Models\Products::sumPerCategory('DATE_FORMAT(purchase.created_at,"%Y")')->get();
                $sumPerProduct = \App\Http\Models\Products::sumPerProduct('DATE_FORMAT(purchase.created_at,"%Y")')->get();

                $timeUnit['unit'] = 'year';
           }
           else if($timeUnitOption == 'Month'){
                if( isset( $startDate,$endDate ) && !empty( $startDate ) && !empty( $endDate ) ){

                    $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod()
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();
                    $sumPerCategory = \App\Http\Models\Products::sumPerCategory()
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();
                    $sumPerProduct = \App\Http\Models\Products::sumPerProduct()
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();
                }
                else{
                    $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod()->get();
                    $sumPerCategory = \App\Http\Models\Products::sumPerCategory()->get();
                    $sumPerProduct = \App\Http\Models\Products::sumPerProduct()->get();
                }

                

                $timeUnit['unit'] = 'month';
           }
           else if($timeUnitOption == 'Day'){

                if( isset( $startDate,$endDate ) && !empty( $startDate ) && !empty( $endDate ) ){

                    $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();
                    $sumPerCategory = \App\Http\Models\Products::sumPerCategory('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();
                    $sumPerProduct = \App\Http\Models\Products::sumPerProduct('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')
                    ->whereRaw("DATE_FORMAT(purchase.created_at,'%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."'")
                    ->get();
                }
                else{
                    $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')->get();
                    $sumPerCategory = \App\Http\Models\Products::sumPerCategory('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')->get();
                    $sumPerProduct = \App\Http\Models\Products::sumPerProduct('DATE_FORMAT(purchase.created_at,"%Y-%m-%d")')->get();
                }


                

                $timeUnit['unit'] = 'day';
           } 
        }
        else{
            $sumPerPeriod = \App\Http\Models\Products::sumPerPeriod()->get();
            $sumPerCategory = \App\Http\Models\Products::sumPerCategory()->get();
            $sumPerProduct = \App\Http\Models\Products::sumPerProduct()->get();

            $timeUnit['unit'] = 'month';
        }
        


        

        $category = \App\Http\Models\Category::all();

        
        /*foreach ($sumPerPeriod as $sumPerPeriodEach) {
            $allMonths[] = ['month'=>date('F',strtotime($sumPerPeriodEach->time_unit)),'year'=>date('Y',strtotime($sumPerPeriodEach->time_unit))];
        }
*/
        foreach ($sumPerPeriod as $sumPerPeriodEach) {
            $timeUnit[] = $sumPerPeriodEach->time_unit;
        }

        
        /*echo "<pre>",print_r($allMonths),"<pre>";*/

        return view('static.pos.salesReportAnthony',compact('sumPerPeriod','sumPerCategory','sumPerProduct','category','timeUnit'));
    } 
}
