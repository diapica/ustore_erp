/*
 * Constant Definitions
 * ---
 * 
 * @BASE_URL	: is the primary website URL
 * @API_URL		: is the API main directory
 * @ONLINE_URL 	: is the directory that user
 * 				  interacts with (normal site,
 * 				  without use) of API
 * @DIR_*		: is the directory folder for
 * 				  each function / module
 * @CMD_*		: is the specific command /
 * 				  action for each function /
 * 				  module
 * 
 */
const BASE_URL 			= 'http://127.0.0.1:8000/';
const API_URL 			= BASE_URL + 'api/';
const ONLINE_URL 		= BASE_URL;

const PRESENCE_TOKEN	= ONLINE_URL + 'evaluation/presence/validate';

let step1  = $("#step1");
let step2  = $("#step2");
let step3  = $("#step3");
let step4  = $("#step4");
let step5  = $("#step5");
let step6  = $("#step6");
let step7  = $("#step7");
let step8  = $("#step8");
let step9  = $("#step9");
let step10 = $("#step10");
let step11 = $("#step11");
let step12 = $("#step12");

let step2a = $("#step2a"); // tap intro
let step2b = $("#step2b"); // tap success
let step2c = $("#step2c"); // tap failed
let step2d = $("#step2d"); // tap requires report
let step2e = $("#step2e"); // validation

let question1  = $('input[name=cboQuestion1]:checked');
let question2  = $('input[name=cboQuestion2]:checked');
let question3  = $('input[name=cboQuestion3]:checked');
let question4  = $('input[name=cboQuestion4]:checked');
let question5  = $('input[name=cboQuestion5]:checked');
let question6  = $('input[name=cboQuestion6]:checked');
let question7  = $('input[name=cboQuestion7]:checked');
let question8  = $('input[name=cboQuestion8]:checked');
let question9  = $('input[name=cboQuestion9]:checked');
let question10 = $('input[name=cboQuestion10]:checked');

let step12_validationProcess = $("#evaluationValidating");
let step12_validationSuccess = $("#evaluationSubmitted");

let txtInputToken = $('#txtInputToken');
let btnLogout = $("#btnLogout");
let btnNext = $("#btnNext");
let cboLogout = $("#cboLogout");
let navButtons = $("#navigation-button");
let answers = Array();

let stepsNow = 1;
let reportContent = null;

$(document).ready(function()
{	
	/*
	 * CSRF Token Initialization
	 * ---
	 * 
	 * CSRF used to prevent unauthorized access / flood to the
	 * system and will be sent with every data
	 * 
	 */
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	/*
	 * Initializer
	 * ---
	 * 
	 * This statement runs at the first time the document is
	 * opened.
	 * 
	 */
	step1.show();

	/*
	 * AJAX Feedback
	 * (EVENT LISTENER)
	 * ---
	 * 
	 * Everytime an AJAX operation is completed wether its valid
	 * or not, user will be notified
	 * 
	 */
	$(document).ajaxComplete(function(){
		$('.alert').delay(1000).fadeOut(400);
    });

    /*
	 * Input Listener
	 * ---
	 * 
	 * This statement runs everytime user types on keyboard.
	 * 
	 */
    $(document).keypress(function(e) {
        if(e.which == 13 && is_trigger_enabled) validate();
    });

})

/*
 * validate()
 * ---
 * 
 * 
 * 
 */

function validate()
{
	is_trigger_enabled = false;
	var data = {
		'presence_token': txtInputToken.val(),
		'report': reportContent,
	}
    
    step2a.hide();
    step2e.fadeIn(100);

	$.ajax({
		type	: "post",
		url		: PRESENCE_TOKEN,
		data	: data,

    	success:function(response)
		{
			if (reportContent)
			{
				if (response['status'] == "TAP_OUT_SUCCESS")
				{
					step12_validationProcess.hide();
					step12_validationSuccess.fadeIn(100);
				}
				else
				{
					alert("Interal Server Error: please note the supervisor that you can't sign in because of system error.");
				}
				
			}
			else
			{
				step2e.hide();
				if (response['status'] == "TAP_OUT_REQUIRES_REPORT")
				{
					allowNext();
					step2d.fadeIn(100);
				}
				else if (response['status'] == "TAP_IN_SUCCESS") step2b.fadeIn(100);
				else if (response['status'] == "INVALID_TOKEN") step2c.fadeIn(100);
			}
		},

		error:function(xhr, textStatus, errorThrown)
		{
			alert("Interal Server Error: please note the supervisor that you can't sign in because of system error.");
		}
	});
}

function nextStep(incr)
{
    btnNext.prop("disabled", true)

    if (incr == 1) stepsNow++;
	else if (incr == -1) stepsNow--;
	
	step1.hide();
    step2.hide();
    step3.hide();
    step4.hide();
    step5.hide();
    step6.hide();
    step7.hide();
    step8.hide();
    step9.hide();
    step10.hide();
    step11.hide();

	if (stepsNow == 1)       step1.fadeIn(500);
	else if (stepsNow == 2)  step2.fadeIn(500);
	else if (stepsNow == 3)  step3.fadeIn(500);
	else if (stepsNow == 4)  step4.fadeIn(500);
	else if (stepsNow == 5)  step5.fadeIn(500);
	else if (stepsNow == 6)  step6.fadeIn(500);
	else if (stepsNow == 7)  step7.fadeIn(500);
	else if (stepsNow == 8)  step8.fadeIn(500);
	else if (stepsNow == 9)  step9.fadeIn(500);
	else if (stepsNow == 10) step10.fadeIn(500);
	else if (stepsNow == 11) step11.fadeIn(500);
	else if (stepsNow == 12)
	{
		step12.fadeIn(500);

		answers.push($('input[name=cboQuestion2]:checked').val());
		answers.push($('input[name=cboQuestion3]:checked').val());
		answers.push($('input[name=cboQuestion4]:checked').val());
		answers.push($('input[name=cboQuestion5]:checked').val());
		answers.push($('input[name=cboQuestion6]:checked').val());
		answers.push($('input[name=cboQuestion7]:checked').val());
		answers.push($('input[name=cboQuestion8]:checked').val());
		answers.push($('input[name=cboQuestion9]:checked').val());
		answers.push($('input[name=cboQuestion10]:checked').val());
		
		reportContent = "<p>Today's report:</p>" +
		"<p>-" + answers[0] + "</p>" +
		"<p>-" + answers[1] + "</p>" +
		"<p>-" + answers[2] + "</p>" +
		"<p>-" + answers[3] + "</p>" +
		"<p>-" + answers[4] + "</p>" +
		"<p>-" + answers[5] + "</p>" +
		"<p>-" + answers[6] + "</p>" +
		"<p>-" + answers[7] + "</p>" +
		"<p>-" + answers[8] ;
		
		validate();
	}
	
	// alert($('input[name=cboQuestion2]:checked').val());

    if (stepsNow > 1 && stepsNow <= 11) navButtons.show();
    else navButtons.hide();
}

function allowLogout()
{
    btnLogout.prop("disabled", false)
    // if (cboLogout.is(":checked").val()) btnLogout.prop("disabled", false);
    // else btnLogout.prop("disabled", true);
}

function allowNext()
{
    btnNext.prop("disabled", false);
}

function logout()
{
    window.location.replace("/accounts/logout");
}

function reset()
{
    step2a.fadeIn(100);
    step2b.hide();
    step2c.hide();
    step2d.hide();
    step2e.hide();
}

function backToHome()
{
    window.location.replace("/");
}