/*
 * Constant Definitions
 * ---
 * 
 * @BASE_URL	: is the primary website URL
 * @API_URL		: is the API main directory
 * @ONLINE_URL 	: is the directory that user
 * 				  interacts with (normal site,
 * 				  without use) of API
 * @DIR_*		: is the directory folder for
 * 				  each function / module
 * @CMD_*		: is the specific command /
 * 				  action for each function /
 * 				  module
 * 
 */
const BASE_URL 			= 'http://127.0.0.1:8000/';
const API_URL 			= BASE_URL + 'api/';
const ONLINE_URL 		= BASE_URL;

const PRODUCT_ADD 		= API_URL + 'product/add';
const PRODUCT_FIND 		= API_URL + 'product/find';
const CART_ADD 			= API_URL + 'pos/cart/add';
const CART_MODIFY		= API_URL + 'pos/cart/modify';
const CART_REFRESH		= API_URL + 'pos/cart/refresh';
const CART_CLEAR		= API_URL + 'pos/cart/clear';
const CART_CHECKOUT		= API_URL + 'pos/cart/checkout';
const CUSTOMER_ADD 		= API_URL + 'customer/add';
const CUSTOMER_FIND 	= API_URL + 'customer/find';

let txtSearchCustomer = $("#txtSearchCustomer");
let lblCustName = $("#lblCustName");
let lblCollegeInfo = $("#lblCollegeInfo");
let lblCustPoints = $("#lblCustPoints");
let frmIntro = $("#step1");
let frmFindingCustomer = $("#step2");
let frmCustomerRegistration = $("#step3");
let frmCustomerDetail = $("#step4");
let frmWizzard = $("#pos-wizzard");
let frmPOS = $("#pos-main");

$(document).ready(function()
{	
	/*
	 * CSRF Token Initialization
	 * ---
	 * 
	 * CSRF used to prevent unauthorized access / flood to the
	 * system and will be sent with every data
	 * 
	 */
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	/*
	 * Initializer
	 * ---
	 * 
	 * This statement runs at the first time the document is
	 * opened.
	 * 
	 */
	frmIntro.show();
	txtSearchCustomer.focus();

	/*
	 * AJAX Feedback
	 * (EVENT LISTENER)
	 * ---
	 * 
	 * Everytime an AJAX operation is completed wether its valid
	 * or not, user will be notified
	 * 
	 */
	$(document).ajaxComplete(function(){
		$('.alert').delay(1000).fadeOut(400);
	})

	/*
	 * Search Product by Name
	 * (EVENT LISTENER)
	 * ---
	 * 
	 * Search product by name which runs on real time everytime
	 * user typed in search box.
	 * 
	 */
	$('#search-item').keyup(function(){
		var search = $(this).val();
		var data = {
			'search':search,
			'type':'name'
		}

		$.ajax({
			type	: "post",
			url		: PRODUCT_FIND,
			data	: data,
			 
			success:function(response)
			{
                $("#search_result").html(response);
            }
		});
	});

	/*
	 * ???
	 * (EVENT LISTENER)
	 * ---
	 * 
	 * ???
	 * 
	 */
	txtSearchCustomer.keypress(function(e)
	{
		if(e.which == 13) findCustomer();
	});

	/*
	 * Add Product to cart by Barcode
	 * (EVENT LISTENER)
	 * ---
	 * Requires:
	 * @product_id
	 * @nim
	 * @type :
	 * 
	 * 
	 */
	$(".barcode").keypress(function(e)
	{
		if(e.which == 13)
		{
			var barcode = $(this).val();
			var data = {
				'barcode'	: barcode,
				'nim':$('#input_nim').val(),
				'type'	: 'barcode'
			}

			addcartAjax(data);
			$(this).val("");
		};
	});

	/*
		*Barcode Textbox always empty when it get focus
	*/
	$(".barcode").focus(function(){
		$(this).val("");
	})
})

/*
 * addCart
 * (ON CLICK FUNCTION)
 * ---
 * 
 * Add product to cart
 * 
 * Requires:
 * @product_id
 * @nim
 * @type : 
 */
function addCart(product_id)
{
	var data = {
		'product_id': product_id,
		'nim'		: $('#input_nim').val(),
		'type'		: 'product_id'
	}

	addcartAjax(data);
	
}

/*
 * 
 * Add product to cart
 * Requires: 
 * data (Got from function addCart / barcode keypress enter)
*/ 

function addcartAjax(data){
	$.ajax({
		type	: "post",
		url		: CART_ADD,
		data	: data,

		beforeSend:function()
		{
			var res = "<div class='alert alert-info'>Data Sedang di proses</div>"
			$("#alert").html(res);  
		},

		success:function(response)
		{
			if(response['status']=="failed"){
				showMessage("Failed to add product to cart", "success");
			}else{
				$("#addProductCart").html(response);
				showMessage("Successfully added to cart", "success");
			}
			
		},

		error:function(xhr, textStatus, errorThrown)
		{
			alert(errorThrown + "---");
			console.log(xhr);
		}
	});
}

function showMessage(message, status)
{
	$.notify({
		icon: 'far fa-check-circle',
		message: "&nbsp&nbsp&nbsp&nbsp&nbsp" + message
	},{
		type: 'success',
		timer: 2000,
		animate: {
			enter: 'animated fadeInDown',
			exit: 'animated fadeOutDown'
		},
		template: '<div data-notify="container" class="col-xs-12 col-sm-5 alert alert-{0}" role="alert">' +
			'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
			'<span style="font-size: 14px; margin-top: -6px;" data-notify="icon"></span> ' +
			'<span data-notify="title">{1}</span> ' +
			'<span data-notify="message">{2}</span>' +
		'</div>' 
	});	
}

/*
 * findCustomer()
 * ---
 * 
 * 
 * 
 */
function findCustomer()
{
	var data = {
		'nim': txtSearchCustomer.val()
	}

	frmIntro.hide();
	frmFindingCustomer.fadeIn(100);

	$.ajax({
		type	: "post",
		url		: CUSTOMER_FIND,
		data	: data,

    	success:function(response)
		{
			frmFindingCustomer.hide();
			if(response==0)
			{
				frmCustomerRegistration.fadeIn(500);
			}
			else
			{
				lblCustName.html("Nama Pelanggan")
				lblCollegeInfo.html("Fakultas, Jurusan Angkatan");
				lblCustPoints.html("0 points");
				frmCustomerDetail.fadeIn(500);
			}
		},

		complete:function()
		{
				refreshCart();
		},

		error:function(xhr, textStatus, errorThrown)
		{
			alert(errorThrown + "---");
			console.log(xhr);
		}
	});
}

function exit()
{
	window.location.replace("/");
}

function cancel()
{
	frmCustomerDetail.hide();
	frmCustomerRegistration.hide();
	frmIntro.fadeIn(500);
	txtSearchCustomer.val("");
	txtSearchCustomer.focus();
}

function sales()
{
	frmWizzard.hide();
	frmPOS.fadeIn(500);
	$("#search-item").focus();
}

/*
 * addCustomer()
 * ---
 * 
 * 
 * 
 */
function addCustomer()
{
	var data = {
		'name': $('#input_nama').val(),
		'angkatan': $('#input_angkatan').val(),
		'email': $('#input_email').val(),
		'fakultas': $('#input_fakultas').val(),
		'jurusan': $('#input_jurusan').val(),
		'nim': $('#input_nim').val()
	}

	$.ajax({
		type	: "post",
		url		: CUSTOMER_ADD,
		data	: data,
	
		beforeSend:function()
		{
			var res = "<div class='alert alert-info'>Data Sedang di proses</div>"
			$("#alert").html(res);  
		},

		success:function(response)
		{
			$('#add_user_dialogue').modal('hide');
			$('#transaction_detail').fadeIn(500);
			$('#sales_card').fadeIn(500);
			$('#find_customer').fadeOut(500);
			$('#datatables').dataTable();
			// $('#find').show();
			// $('#nama').prop('disabled',true);
			// $('#email').prop('disabled',true);
			// $('.daftar').hide();
			// $('.search').removeAttr('disabled');
			// $('.fresh-datatables').show();
			// $('#addProductCart').show();
		},

		complete:function()
		{
			refreshCart();
		},

		error:function(xhr, textStatus, errorThrown)
		{
			alert(errorThrown + "---");
			console.log(xhr);
		}
	});
}

/*
 * discount()
 * ---
 * 
 * 
 * 
 */
function discount()
{
	var diskon = $('.discount').val();
	var subtotal = $('.subtotal').attr('subtotal');
	var grandtotal = subtotal-diskon;

	var	reverse = diskon.toString().split('').reverse().join('');
	var harga_diskon = reverse.match(/\d{1,3}/g);
	harga_diskon = harga_diskon.join('.').split('').reverse().join('');
	diskon = 'Rp. '+harga_diskon
	$('.disc').html(diskon);
	
	var	reverse = grandtotal.toString().split('').reverse().join('');
	var grandtotal = reverse.match(/\d{1,3}/g);
	grandtotal = grandtotal.join('.').split('').reverse().join('');
	grand_total = 'Rp. '+grandtotal;
	$('.grandtotal').html(grand_total);

}

/*
 * changeItemSum()
 * ---
 * 
 * 
 * 
 */
function changeItemSum(status,id)
{
	$.ajax({
		type	: "post",
		url		: CART_MODIFY + '/' + id + '/' + status,

		beforeSend:function()
		{
			var res = "<div class='alert alert-info'>Data Sedang di proses</div>"
			$("#alert").html(res);  
		},

		success:function(response)
		{
			if(response['status']=="failed"){
				showMessage("Failed to add product to cart", "success");
			}else{
				$("#addProductCart").html(response);
				showMessage("Successfully Modify The cart", "success");
			}
		}
	});
}

/*
 * refreshCart()
 * ---
 * 
 * 
 * 
 */
function refreshCart()
{
	var nim = $('#input_nim').val()
	var data = {
		'nim':nim
	}

	$.ajax({
		type:"post",
		url:CART_REFRESH,
		data:data,

		beforeSend:function()
		{
			var res = "<div class='alert alert-info'>Data Sedang di proses</div>"
			$("#alert").html(res);  
		},

		success:function(response)
		{
			$("#addProductCart").html(response);
		}
	});

}

/*
 * clearCart()
 * ---
 * 
 * 
 * 
 */
function clearCart(TempID)
{
	var data = {
		'temp_purchase_id':TempID
	}

	$.ajax({
		type	: "post",
		url		: CART_CLEAR,
		data	: data,

		beforeSend:function()
		{
			var res = "<div class='alert alert-info'>Data Sedang di proses</div>"
			$("#alert").html(res);  
		},

		success:function(response)
		{
			$("#addProductCart").html(response);
		}
	});
}

/*
 * proses()
 * ---
 * 
 * 
 * 
 */
function proses()
{
	var nim = $('#input_nim').val();
	var disc = $('.discount').val();
	var tax = 0;
	data = nim+"|"+disc+"|"+tax;
	location.href = CART_CHECKOUT +"/"+data;
}