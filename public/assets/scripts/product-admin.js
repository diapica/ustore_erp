$(document).ready(function(){

	$('.delete').click(function(){
		var product_name = $(this).attr('product_name');
		var konfirmasi = confirm("Are you sure to delete "+product_name+" ?");
		if (konfirmasi==true){
			var id = $(this).attr('product_id');
			var baseURL = window.location.href;
			var baseurl = baseURL.split('/products');
			var indexURL = baseurl[0];
			var url = indexURL+'/products/delete/product/'+id;
			location.href = url;
		}
	})

});