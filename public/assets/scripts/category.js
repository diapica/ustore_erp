$(document).ready(function(){
	
	$('.delete').click(function(){
		var category = $(this).attr('category');
		var conf = confirm('Are you sure want to delete '+category+" category ?")
		if(conf==true){
			var id = $(this).attr('category_id');
			var baseURL = window.location.href;
			var url = baseURL+"/delete/"+id;
			location.href = url
		}
	})

	$('.edit').click(function(){
		var category = $(this).attr('category');
		var id = $(this).attr('category_id');
		$('#category').val(category);
		$('#sembunyi').val(id);
		$('#cmd').val('Update')
	})

})