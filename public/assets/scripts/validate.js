/*
 * Constant Definitions
 * ---
 * 
 * @BASE_URL	: is the primary website URL
 * @API_URL		: is the API main directory
 * @ONLINE_URL 	: is the directory that user
 * 				  interacts with (normal site,
 * 				  without use) of API
 * @DIR_*		: is the directory folder for
 * 				  each function / module
 * @CMD_*		: is the specific command /
 * 				  action for each function /
 * 				  module
 * 
 */
const BASE_URL 			= 'http://127.0.0.1:8000/';
const API_URL 			= BASE_URL + 'api/';
const ONLINE_URL 		= BASE_URL;

const PRESENCE_TOKEN	= ONLINE_URL + 'evaluation/presence/validate';

$(document).ready(function()
{	
	/*
	 * CSRF Token Initialization
	 * ---
	 * 
	 * CSRF used to prevent unauthorized access / flood to the
	 * system and will be sent with every data
	 * 
	 */
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	/*
	 * Initializer
	 * ---
	 * 
	 * This statement runs at the first time the document is
	 * opened.
	 * 
	 */
	$('#logged_form').hide();
	$('#success_form').hide();
	$('#report_form').hide();
	$("#input_token").focus();

	/*
	 * AJAX Feedback
	 * (EVENT LISTENER)
	 * ---
	 * 
	 * Everytime an AJAX operation is completed wether its valid
	 * or not, user will be notified
	 * 
	 */
	$(document).ajaxComplete(function(){
		$('.alert').delay(1000).fadeOut(400);
	})

})

/*
 * validate()
 * ---
 * 
 * 
 * 
 */
function validate()
{
	var data = {
		'presence_token': $('#input_token').val(),
		'report': $('#report').val(),
	}

	$.ajax({
		type	: "post",
		url		: PRESENCE_TOKEN,
		data	: data,

		beforeSend:function()
		{
			var res = "<div class='alert alert-info'>Data Sedang di proses</div>"
      		$("#alert").html(res);  
		},

    	success:function(response)
		{
			if (response['status'] == "TAP_OUT_SUCCESS")
			{
				$('#success_form').fadeIn(500);
				$('#report_form').fadeOut(500);
			}
			else if (response['status'] == "TAP_OUT_REQUIRES_REPORT")
			{
				$('#report_form').fadeIn(500);
				$('#check_in').fadeOut(500);
				$('#report').focus();
			}
			else if (response['status'] == "TAP_IN_SUCCESS")
			{
				$('#logged_form').fadeIn(500);
				$('#check_in').fadeOut(500);
			}
			else if (response['status'] == "INVALID_TOKEN")
			{
				showMessage("You've entered invalid credentials. Please check if your credential match with the currently logged in user.", "danger");
			}
		},

		error:function(xhr, textStatus, errorThrown)
		{
			alert(errorThrown + "---");
			console.log(xhr);
		}
	});
}

function showMessage(message, status)
{
	let xicon = '';
	if (status == "success") xicon = "fas fa-check";
	else if (status == "danger") xicon = "fas fa-times";

	$.notify({
		message: message
	},{
		type: status,
		animate: {
			enter: 'animated fadeInDown',
			exit: 'animated fadeOutDown'
		},
	});	
}