/*
 * Constant Definitions
 * ---
 * 
 * @BASE_URL	: is the primary website URL
 * @API_URL		: is the API main directory
 * @ONLINE_URL 	: is the directory that user
 * 				  interacts with (normal site,
 * 				  without use) of API
 * @DIR_*		: is the directory folder for
 * 				  each function / module
 * @CMD_*		: is the specific command /
 * 				  action for each function /
 * 				  module
 * 
 */
const BASE_URL 			= 'http://127.0.0.1:8000/';
const API_URL 			= BASE_URL + 'api/';
const ONLINE_URL 		= BASE_URL;

const PRESENCE_TOKEN	= ONLINE_URL + 'evaluation/presence/validate';

let is_trigger_enabled  = true;

let frmFailed = $('#frmFailed');
let frmSuccess = $('#frmSuccess');
let frmTapout = $('#frmTapout');
let frmValidation = $('#frmValidation');
let frmInputToken = $('#frmInputToken');
let txtInputToken = $('#txtInputToken');

$(document).ready(function()
{	
	/*
	 * CSRF Token Initialization
	 * ---
	 * 
	 * CSRF used to prevent unauthorized access / flood to the
	 * system and will be sent with every data
	 * 
	 */
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	/*
	 * Initializer
	 * ---
	 * 
	 * This statement runs at the first time the document is
	 * opened.
	 * 
	 */
	frmFailed.hide();
    frmValidation.hide();
	frmSuccess.hide();
	frmTapout.hide();
	txtInputToken.focus();

	/*
	 * AJAX Feedback
	 * (EVENT LISTENER)
	 * ---
	 * 
	 * Everytime an AJAX operation is completed wether its valid
	 * or not, user will be notified
	 * 
	 */
	$(document).ajaxComplete(function(){
		$('.alert').delay(1000).fadeOut(400);
    });

    /*
	 * Input Listener
	 * ---
	 * 
	 * This statement runs everytime user types on keyboard.
	 * 
	 */
    $(document).keypress(function(e) {
        if(e.which == 13 && is_trigger_enabled) validate();
    });

})

/*
 * validate()
 * ---
 * 
 * 
 * 
 */

function validate()
{
	is_trigger_enabled = false;
	var data = {
		'presence_token': txtInputToken.val(),
		'report': null,
	}
    
    frmInputToken.hide();
    frmValidation.fadeIn(100);

	$.ajax({
		type	: "post",
		url		: PRESENCE_TOKEN,
		data	: data,

    	success:function(response)
		{
			frmValidation.hide();
			if (response['status'] == "TAP_OUT_REQUIRES_REPORT")
			{
				frmTapout.fadeIn(100);
			}
			else if (response['status'] == "TAP_IN_SUCCESS")
			{
                frmSuccess.fadeIn(100);
			}
			else if (response['status'] == "INVALID_TOKEN")
			{
				frmFailed.fadeIn(100);
			}
		},

		error:function(xhr, textStatus, errorThrown)
		{
			alert("Interal Server Error: please note the supervisor that you can't sign in because of system error.");
		}
	});
}

function reset()
{
	is_trigger_enabled = true;
	frmFailed.hide();
	frmSuccess.hide();
	frmTapout.hide();
	frmValidation.hide();
	frmInputToken.fadeIn(100);
}