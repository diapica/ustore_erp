<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Vendor Control Panel</title>
	<link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
</head>
<body>
	<div class="container">
		<h2>Create A Product</h2><br>
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div><br>
		@endif
		@if (\Session::has('success'))
		<div class="alert alert-success">
			<p>{{ \Session::get('success') }}</p>
		</div><br>
		@endif
		<form method="post" action="/vendor/create">
			{{ csrf_field() }}
			<div class="row">
				<div class="col-md-4"></div>
				<div class="form-group col-md-4">
					<label for="barcode">Barcode:</label>
					<input type="numeric" class="form-control" name="barcode">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"></div>
				<div class="form-group col-md-4">
					<label for="name">Name:</label>
					<input type="text" class="form-control" name="name">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"></div>
				<div class="form-group col-md-4">
					<label for="stock">Stock:</label>
					<input type="number" class="form-control" name="stock">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"></div>
				<div class="form-group col-md-4">
					<label for="price">Price:</label>
					<input type="numeric" class="form-control" name="price">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"></div>
				<div class="form-group col-md-4">
					<button type="submit" class="btn btn-success" style="margin-left: 80px">Add Product</button>
					<a class="btn btn-success" href="/vendor">Back</a>
				</div>
			</div>
		</form>
	</div>
</body>
</html>