<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Vendor Control Panel</title>
	<link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
</head>
<body>
	<div class="container">
		<h2>Vendor's Product</h2><br>
		@if (\Session::has('success'))
		<div class="alert alert-success">
			<p>{{ \Session::get('success') }}</p>
		</div><br>
		@endif
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Barcode</th>
					<th>Name</th>
					<th>Stock</th>
					<th>Price</th>
					<th colspan="2">Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($products as $product)
				<tr>
					<td>{{ $product['id'] }}</td>
					<td>{{ $product['barcode'] }}</td>
					<td>{{ $product['name'] }}</td>
					<td>{{ $product['stock'] }}</td>
					<td>{{ $product['price'] }}</td>
					<td><a href="{{ action('VendorController@edit', $product['id']) }}" class="btn btn-warning">Edit</a></td>
					<td>
						<form action="{{ action('VendorController@destroy', $product['id']) }}" method="post">
							{{ csrf_field() }}
							{{method_field('DELETE')}}
							<input name="_method" type="hidden" value="DELETE">
							<button class="btn btn-danger" type="submit">Delete</button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<div class="text-center">
			<a class="btn btn-success" href="/vendor/create"> Create A Product</a>
        </div>
	</div>
</body>
</html>