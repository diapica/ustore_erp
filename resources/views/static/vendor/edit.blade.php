<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Vendor Control Panel</title>
	<link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
</head>
<body>
	<div class="container">
		<h2>Edit A Product</h2><br>
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div><br>
		@endif
		<form method="POST" action="/vendor/update/{{$product->id}}">
			{{csrf_field()}}
			{{method_field('PUT')}}
			<div class="row">
				<div class="col-md-4"></div>
				<div class="form-group col-md-4">
					<label for="barcode">Barcode:</label>
					<input type="numeric" class="form-control" name="barcode" value="{{ $product->barcode }}">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"></div>
				<div class="form-group col-md-4">
					<label for="name">Name:</label>
					<input type="text" class="form-control" name="name" value="{{ $product->name }}">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"></div>
				<div class="form-group col-md-4">
					<label for="stock">Stock:</label>
					<input type="number" class="form-control" name="stock" value="{{ $product->stock }}">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"></div>
				<div class="form-group col-md-4">
					<label for="price">Price:</label>
					<input type="numeric" class="form-control" name="price" value="{{ $product->price }}">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"></div>
				<div class="form-group col-md-4">
					<button type="submit" class="btn btn-success" style="margin-left:38px">Update Product</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>