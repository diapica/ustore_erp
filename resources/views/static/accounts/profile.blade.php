@extends('globals.base')

@section('scripts')


@endsection

@section('contents')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div class="card">
                <div class="card-header">My Profile</div>
                <div class="card-body">
                    <div class="row">
                            <div class="col-lg-4 col-md-5">
                                <div class="author text-center">
                                    <img class="avatar border-white" src="{{$image}}" alt="Image" id="profile" height="200 px" width="200 px"/><br /><br />
                                    {{--<label class="btn btn-info btn-wd text-white">
                                    <input type="file" name="profile_image" id="avatar" style="display:none"; onchange="document.getElementById('profile').src = window.URL.createObjectURL(this.files[0])">
                                        Change Avatar
                                    </label>--}}
                                    <a href="/changePassword" class="btn btn-default">Change Password</a>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-7">
                                <div class="content">
                                <form action= "/saveProfile" method="POST">
                                        {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Username</label>
                                                <input type="text" name="name" class="form-control border-input" placeholder="Username" value="{{$user->name}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>NIM</label>
                                                <input type="numeric" name="nim" class="form-control border-input" placeholder="NIM" value="{{$user->nim}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email Address</label>
                                                <input type="email" name="email" class="form-control border-input" placeholder="Email Address" value="{{$user->email}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Fakultas</label>
                                                <input type="text" name="fakultas" class="form-control border-input" placeholder="Fakultas" value="{{$user->fakultas}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Jurusan</label></br>
                                                <input type="text" name="jurusan" class="form-control border-input" placeholder="Jurusan" value="{{$user->jurusan}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Birth Date</label>
                                                <input type="date" name="date" class="form-control border-input" placeholder="Date" value="{{$user->date}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Angkatan</label></br>
                                                <input type="numeric" name="angkatan" class="form-control border-input" placeholder="Angkatan" value="{{$user->angkatan}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>About Me</label>
                                                <textarea rows="5" name="about" class="form-control border-input" placeholder="Tell about Yourself">{{$user->about}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success btn-fill btn-wd">Update Profile</button>
                                        <button type="reset" class="btn btn-danger btn-fill btn-wd">Cancel</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
@endsection
