<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Ustore ERP — Login</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="assets/styles/fontawesome-all.min.css" rel="stylesheet" />
    <link href="assets/styles/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/styles/style.css" rel="stylesheet" />
</head>

<body>
    <div class="wrapper login-form">
        <div class="login-container">
            <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <h3>Grab a coffee and let's get to work!</h3>
                <p>Siapkah kamu melayani kembali pelanggan di hari yang indah ini?</p>
                <p>Hai, kamu yang bukan pengurus dan melihat halaman ini, <i>stay tune</i> dan tunggu <a href="#">informasi mengenai karir</a> di Ustore!</p>
                <div class="login-control">
                    <input class="form-control" type="email" placeholder="Enter email" name="email" value="{{ old('email') }}" required autofocus>
                    <input class="form-control" type="password" placeholder="Password" name="password" required>
                </div>
                {{-- <!-- Email -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    
                    @if ($errors->has('email'))
                        <span class="login-error">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <!-- Password -->
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    
                    @if ($errors->has('password'))
                        <span class="login-error">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div> --}}
                <p><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me</p>
                <p class="m-bottom-0"><button type="submit" class="btn btn-info btn-long m-bottom-0">Login</button></p>
            </form>
        </div>
    </div>
</body>

<script src="assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/theme.js" type="text/javascript"></script>

</html>