<!DOCTYPE html>

<html lang="id">

<head>
    <title>Print All Barcode</title>
</head>

<body style="font-family: open sans, tahoma, sans-serif; margin: 0; -webkit-print-color-adjust: exact" onload="window.print()">

    <div style="width: 790px;">

        <table width="790" cellspacing="10" cellpadding="10" class="container" style="width: 790px; padding: 20px;">
            <tbody>
                    @foreach($products as $key => $product)
                        @if($key%4 == 0)
                            <tr style="margin-top: 20px; margin-bottom: 20px;">
                        @endif
                        <td style="text-align:center;">
                              <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($product['barcode'], 'C39E',1,55)}}" alt="barcode" />
                            <br> {{$product['barcode']}} - {{$product['product_name']}}                            
                        </td>
                        <!-- {{$key++}} -->
                        @if($key%4 == 0)       
                            </tr>
                        @endif
                    @endforeach          
            </tbody>
        </table>
    </div>
</body>

</html>