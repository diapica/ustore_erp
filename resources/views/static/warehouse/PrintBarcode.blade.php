<!DOCTYPE html>

<html lang="id">

<head>
    <title>{{$barcode}} - {{$product_name}}</title>
</head>

<body style="font-family: open sans, tahoma, sans-serif; margin: 0; -webkit-print-color-adjust: exact" onload="window.print()">

    <div style="width: 790px;">

        <table width="790" cellspacing="10" cellpadding="10" class="container" style="width: 790px; padding: 20px;">
            <tbody>
                <tr style="">
                    @for($y=1;$y<=3;$y++)
                        <td style="text-align:center;">
                              <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($barcode, 'C39E',1,55)}}" alt="barcode" />
                            <br> {{$barcode}} - {{$product_name}}                            
                        </td>          
                    @endfor          
                </tr>
                @for($x=1;$x<=8;$x++)
               <tr style="margin-top: 20px; margin-bottom: 20px;">
                    @for($y=1;$y<=3;$y++)
                        <td style="text-align:center;">
                              <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($barcode, 'C39E',1,55)}}" alt="barcode" />
                            <br> {{$barcode}} - {{$product_name}}                            
                        </td>          
                    @endfor   
                </tr>
                @endfor
            </tbody>
        </table>
    </div>
</body>

</html>