@extends('globals.base')
<!-- JS File -->
<script src=" {{ asset('/assets/js/core/jquery.3.2.1.min.js') }} " type="text/javascript"></script>
<script src="{{ asset('/dev/js/category.js') }}"></script>
@section('contents')
<div class="row">


  <!-- Begin sales table -->
  <div class="col-md-12">
    <div class="card data-tables">

      <div class="card-header">
        <div class="row">
          <div class="col-md-12">
            <h4 class="card-title">Category</h4>
          </div>
        </div>
      </div>

      <div class="card-body">
                               <div class="toolbar">
                                    <div class="container-fluid">
                                      <div class="row">
                                        <div class=" col-md-6 "></div>
                                                  <div class=" col-md-6 ">
            <form role="form" id='form' name='form' method="POST" action={{ url( '/category/save') }} enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group ">
                <label>Category :</label>
                <input type="text" name="category" id="category" class="form-control">
              </div>

              <div class="form-group float-right">
                <input type="submit" id="cmd" name="cmd" class="btn btn-md btn-info pull-right" onclick='save()' value="Add Category">
              </div>
              <input type='hidden' id='sembunyi' name='sembunyi'>
            </form>
          </div>
                                      </div>
                                    </div>
                              </div>
        <hr class="m-all-0" />
        <div class="fresh-datatables">

          <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
              <tr>
                <th>No</th>
                <th>Category</th>
                <th class="disabled-sorting">Actions</th>
              </tr>
            </thead>
            <tbody>
              @if(count($categories)==0)
              <tr>
                <td colspan='3' style="text-align:center"> There's No Category in Here</td>
                <td></td>
              </tr>
              @else @foreach($categories as $key => $category)
              <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $category['category'] }}</td>
                <td>
                  <button type="button" class="btn btn-info btn-sm edit" category_id="{{ $category['id'] }}" category="{{ $category['category'] }}">
                          <span class="btn-label">
                            <i class="far fa-edit"></i>
                          </span>
                          Edit
                        </button>

                  <button type="button" class="btn btn-danger btn-sm delete" category_id="{{ $category['id'] }}" category="{{ $category['category'] }}">
                        <span class="btn-label">
                          <i class="far fa-trash"></i>
                        </span>
                        Delete
                      </button>
                </td>
              </tr>
              @endforeach @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
