@extends('globals.base')
<!-- JS File -->
<script src=" {{ asset('/assets/js/core/jquery.3.2.1.min.js') }} " type="text/javascript"></script> 
<script src= "{{ asset('/dev/js/product-admin.js') }}"></script>
@section('contents')
  <div class="row">


    <!-- Begin sales table -->
    <div class="col-md-12">
      <div class="card data-tables">
        <div class="card-header">
          <h4 class="card-title">Products</h4>
          		<button type="button" class="btn btn-info float-right" data-toggle="modal" data-target="#AddProducts">
					<span class="btn-label"><i class="far fa-plus"></i></span>
					Add New Products
			  	</button>
				<a href="{{ url('/products/barcode/print','all') }}" class="btn btn-info float-right m-right-5">
					<span class="btn-label"><i class="far fa-print"></i></span>
					Print All Products Barcode
				</a>
          <br>
        </div>
          <hr class="m-all-0" />
          <div class="fresh-datatables">
            <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Stock</th>
                  <th>Price</th>
                  <th>Vendor Name</th>
                  <th>Category</th>
                  <th>Status </th>
                  <th class="disabled-sorting text-right">Actions</th>
                </tr>
              </thead>
              <tbody>
                @if(count($products)==0)
                  <tr>
                    <td colspan='7' style="text-align: center;width:100% "> There's No Product in Here</td>
                    <td></td>
                  </tr>
                @else
                  @foreach($products as $product)
                  <tr>
                    <td>{{ $product['product_name'] }}</td>
                    <td>{{ $product['stock'] }}</td>
                    <td>{{ $product['price'] }}</td>
                    <td>{{ $product['vendor_id'] }}</td>
                    <td>{{ $product->category['category'] }}</td>
                    <td>
                        @if($product['status']==1)
                          Available
                        @else
                          Not Available
                        @endif
                    </td>
                    <td class="text-right">
                      <a  href="{{ url('/products',[ 'id'=>$product['id'], 'name'=>$product['product_name'] ]) }}">
                        <button type="button" class="btn btn-info btn-sm update" product_id ="{{ $product['id'] }}" product_name="{{ $product['product_name'] }}">
                        <span class="btn-label">
                          <i class="far fa-info"></i>
                        </span>
                        See Detail
                      </button>
                    </a>
                    </td>
                  </tr>
                  @endforeach 
                @endif
              </tbody>
            </table>
          </div>
           <div class="card-footer text-center">
              <div class="btn-group">
                  {{ $products->links() }}
              </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Sales Table -->

  <!-- Modal Start -->
   <div class="modal fade" id="AddProducts" role="dialog" >
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labeltambahproduk">Add Products</h5>
          <button type="button" class="close btn-sm" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times"></span></button>
        </div>

        <div class="modal-body">
          <form role="form" id='form' name='form' method="POST" action={{ url('/products/save') }} enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group ">
              <label>Product Name</label>
              <input type="text" name="product_name" id="product_name" class="form-control">
            </div>
            <div class="form-group ">
              <label>Stock</label>
              <input type="number" min="0" name="stock" id="stock" class="form-control">
            </div>
            <div class="form-group ">
              <label>Price</label>
              <input type="text" name="price" id="price" class="form-control">
            </div>
             <div class="form-group ">
              <label>Category</label>
              <select class="form-control" name="category">
                <option value="" disabled="" selected="">Category</option>
                @foreach($categories as $category)
                <option value="{{ $category['id'] }}">{{ $category['category'] }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group" >
              <label>Vendor Name</label>
              <select class="form-control" name="vendor"> 
                <option value="" disabled="" selected="">Vendor Name</option>
                <option value="1">Staples</option>
                <option value="2">BCA</option>
                <option value="3">Libro</option>
              </select>
            </div>
            <div class="form-group ">
              <label>Description</label>
                <textarea name="desc" id="desc" class="form-control col-sm-12" style="margin-top: 0px; margin-bottom: 0px; height: 83px;"></textarea>
            </div>
            <div class="form-group ">
              <label>Status</label>
              <select class="form-control" name="status">
                <option value="" disabled="" selected="">Status</option>
                <option value="0" selected >Not Available</option>
                <option value="1">Available</option>
              </select>
            </div>
            <hr>
            <div class="form-group ">
                <input type="submit" id="cmd" name="cmd" class="btn btn-md btn-success pull-right" onclick='save()'  value="Add Product">
            </div>
          </form>
            <input type='hidden' id='sembunyi'>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal End -->
@endsection