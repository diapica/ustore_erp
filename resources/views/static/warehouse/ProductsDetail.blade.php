@extends('globals.base')
<!-- JS File -->
<script src=" {{ asset('/assets/js/core/jquery.3.2.1.min.js') }} " type="text/javascript"></script>
<script src="{{ asset('/dev/js/product-admin.js') }}"></script>
@section('contents')
<div class="row">


  <!-- Begin sales table -->
  <div class="col-md-12">
    <div class="card data-tables">
      <div class="card-header">
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <h4 class="card-title">Detail {{ $product_name }}</h4>
          </div>
          <div class="col-md-6 col-sm-12 text-right">
            <a href=" {{ url('/products') }} ">
           <button type="button" class="btn btn-info mr-1">
            <span class="btn-label">
              <i class="far fa-arrow-left"></i>
            </span>
              Back To Products
          </button>
          </a>
            <button type="button" class="btn btn-danger delete" product_name="{{ $product_name }}" product_id="{{ $product_detail['id'] }}">
            <span class="btn-label">
              <i class="far fa-trash"></i>
            </span>
              Delete
          </button>
          </div>
        </div>
        <hr/>
      </div>

      <div class="card-body">
        <div class="row">

          <div class="col-md-6">
            <form role="form" id='form' name='form' method="POST" action={{ url( '/products/save') }} enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="row">
                  <label class="col-sm-3 control-label">Product Name</label>
                  <div class="col-sm-9">
                    <input type="text" name="product_name" id="product_name" class="form-control" value="{{ $product_detail['product_name'] }}">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="col-sm-3 control-label">Stock</label>
                  <div class="col-sm-9">
                    <input type="number" min="0" name="stock" id="stock" class="form-control" value="{{ $product_detail['stock'] }}">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="col-sm-3 control-label">Price</label>
                  <div class="col-sm-9">
                    <input type="text" name="price" id="price" class="form-control" value="{{ $product_detail['price'] }}">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="col-sm-3 control-label">Category</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="category">
                      <option value="" disabled selected>Category</option>
                       @foreach($categories as $category)
                      <option value="{{ $category['id'] }}"
                      {{ $product_detail['category_id'] == $category['id'] ? 'selected':''}}>
                      {{ $category['category'] }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="col-sm-3 control-label">Vendor Name</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="vendor">
                      <option value="" disabled  selected >Vendor Name</option>
                      <option value="1" {{ $product_detail['vendor_id'] == 1 ? 'selected':''}}  >Staples</option>
                      <option value="2" {{ $product_detail['vendor_id'] == 2 ? 'selected':''}} >BCA</option>
                      <option value="3" {{ $product_detail['vendor_id'] == 3 ? 'selected':''}} >Libro</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="col-sm-3 control-label">Description</label>
                  <div class="col-sm-9">
                    <textarea name="desc" id="desc" class="form-control col-sm-12" style="margin-top: 0px; margin-bottom: 0px; height: 83px;">{{ $product_detail['desc'] }}</textarea>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="col-sm-3 control-label">Status</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="status">
                      <option value="" disabled>Status</option>
                      <option value="0" {{ $product_detail['status'] == 0 ? 'selected':''}} >Not Available</option>
                      <option value="1" {{ $product_detail['status'] == 1 ? 'selected':''}}>Available</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group text-right">
                <input type="submit" id="cmd" name="cmd" class="btn btn-md btn-info text-right" value="Update Product">
              </div>
          </div>

          <div class="col-md-6">
			  	<div class="row">
					<div class="col-md-12">
						<label class="control-label">Barcode</label>
					</div>
					<div class="col-md-12">
						<img src="data:image/png;base64,{{DNS1D::getBarcodePNG($product_detail['barcode'], 'C39E',1,55)}}" alt="barcode" />
					</div>
					<div class="col-md-12 m-top-10">
						<a target="_blank" href="{{ url('products/barcode/print',$product_detail['id']) }}" class="btn btn-md btn-primary" >
							<span class="btn-label"><i class="fa fa-print"></i></span>
							Print Barcode
						</a>
					</div>
				</div>

              <hr>
              <div class="row">
				  <div class="col-md-12">
                <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Updated By </th>
                      <th>Stock Before</th>
                      <th>Stock After</th>
                      <th>Updated At</th>
                    </tr>
                    <tbody>
                      @if(count($stocks_history)==0)
                      <tr>
                        <td colspan='5' style="text-align: center;width:100% "> There's No Stock Update</td>
                        <td></td>
                      </tr>
                      @else @foreach($stocks_history as $key => $stock_history)
                      <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $stock_history->Users['name'] }}</td>
                        <td>{{ $stock_history['before'] }}</td>
                        <td>{{ $stock_history['after'] }}</td>
                        <td>{{ date('d-F-Y H:i:s',strtotime($stock_history['created_at'])) }}</td>
                      </tr>
                      @endforeach @endif
                    </tbody>
                  </thead>
				</table>
			</div>
                <input type='hidden' id='sembunyi' name="sembunyi" value="{{ $product_detail['id'] }}">
                </form>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
<!-- End Sales Table -->

@endsection
