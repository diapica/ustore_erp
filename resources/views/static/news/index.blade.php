@extends('globals.base')

@section('contents')
	
	@if ($errors->any())
	    <!-- <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div> -->

	    <div class="alert alert-warning alert-dismissible fade show">
	      <strong>Upps!</strong> You should check in on some of those fields below.
	      <ul>
	          @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	          @endforeach
	      </ul>
	      <button type="button" class="close" data-dismiss="alert">
	        <span>&times;</span>
	      </button>
	    </div>
	@endif

	<div class="row">
		<div class="col-12">

			<div class="card data-tables p-4">

				<!-- card-header -->
				<div class="card-header">
					<h4 class="card-title" >News</h4>

					<button type="button" class="btn btn-info float-right" data-toggle="modal" data-target="#AddNews">
					  <span class="btn-label"> <i class="far fa-plus"></i> </span> Add News
					</button>

					<!-- modal -->
					<div class="modal fade" id="AddNews">

					  <div class="modal-dialog">

					    <div class="modal-content">

					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Add News</h5>
					        <button type="button" class="close" data-dismiss="modal">
					          <span>&times;</span>
					        </button>
					      </div>

					      <div class="modal-body">
					        <form method="POST" action="/news">
					        	{{csrf_field()}}
					          <div class="form-group">
					            <label for="subject">Subject:</label>
					            <input type="text" class="form-control" id="subject" name="subject">
					          </div>
					          <div class="form-group">
					            <label for="content">Content:</label>
					            <textarea class="form-control" id="content" rows="10" name="content"></textarea>
					          </div>

					          <button type="submit" class="btn btn-primary">Add News</button>
					        </form>
					      </div>


					    </div>

					  </div>

					</div>
					<!-- /modal -->

				</div>
				<!-- /card-header -->

				<hr class="mt-3 mb-0" />

				<div class="fresh-datatables">
					<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
						<thead>
						  <tr>
						    <th>No</th>
						    <th>Subject</th>
						    <th class="disabled-sorting">Actions</th>
						  </tr>
						</thead>

						<tbody>
							@if(count($news))
								@foreach($news as $new)
									<tr>
										<td>{{$new->id}}</td>
										<td>{{$new->subject}}</td>
										<td>
										  <button type="button" class="btn btn-info btn-sm edit" data-toggle="modal" data-target="#EditNews" id="BtnEditNews" onclick="loadNews('{{$new->subject}}','{{$new->content}}')">
										          <span class="btn-label"> <i class="far fa-edit"></i> </span> Edit
										  </button>


										  <!-- modal -->
										  <div class="modal fade" id="EditNews">

										    <div class="modal-dialog">

										      <div class="modal-content">

										        <div class="modal-header">
										          <h5 class="modal-title" id="exampleModalLabel">Edit News</h5>
										          <button type="button" class="close" data-dismiss="modal">
										            <span>&times;</span>
										          </button>
										        </div>

										        <div class="modal-body">
										          <form method="POST" action="/news/{{$new->id}}">
										          	{{csrf_field()}}
										          	{{method_field('PUT')}}
										            <div class="form-group">
										              <label for="updateSubject">Subject:</label>
										              <input type="text" class="form-control" id="updateSubject" name="subject">
										            </div>
										            <div class="form-group">
										              <label for="updateContent">Content:</label>
										              <textarea class="form-control" id="updateContent" rows="10" name="content"></textarea>
										            </div>

										            <button type="submit" class="btn btn-primary">Edit News</button>
										          </form>
										        </div>


										      </div>

										    </div>

										  </div>
										  <!-- /modal -->

										  <button type="button" class="btn btn-danger btn-sm delete" data-toggle="modal" data-target="#DeleteNews">
										        <span class="btn-label"> <i class="far fa-trash"></i> </span> Delete
										  </button>

										  <!-- modal -->
										  <div class="modal fade" id="DeleteNews">

										    <div class="modal-dialog">

										      <div class="modal-content">

										        <div class="modal-header">
										          <h5 class="modal-title" id="exampleModalLabel">Delete Confirmation</h5>
										          <button type="button" class="close" data-dismiss="modal">
										            <span>&times;</span>
										          </button>
										        </div>

										        <div class="modal-body">
										        	<p>Are you sure want to delete this news?</p>
										          <form method="POST" action="/news/{{$new->id}}">
										          	{{csrf_field()}}
										          	{{method_field('DELETE')}}
										            
										            <button type="submit" class="btn btn-primary">Yes</button>
										          </form>
										        </div>


										      </div>

										    </div>

										  </div>
										  <!-- /modal -->
										</td>
									</tr>

									
								@endforeach
							@else
								<tr>
									<td colspan='3' style="text-align:center">No News Found</td>
									<td></td>
								</tr>
							@endif	
						</tbody>
					</table>
				</div>

				<div class="card-footer text-center">
					
				</div>

			</div>

		</div>
	</div>
	

@endsection
<script src=" {{ asset('/assets/js/core/jquery.3.2.1.min.js') }} " type="text/javascript"></script> 
<script type="text/javascript">

	/*$(document).ready(function(){

	   	$('#BtnEditNews').click(function() {
	   		 $("#updateSubject").val('');
	         $("#updateContent").val('');
	         var subject = $(this).attr('subject');
	         var content = $(this).attr('content');
	         $("#updateSubject").val(subject);
	         $("#updateContent").val(content);
	         console.log(subject);
	       });


	});*/

	function loadNews(subject,content){
		console.log(subject);
		$("#updateSubject").val(subject);
		$("#updateContent").val(content);
	}

	
</script>