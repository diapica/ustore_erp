@extends('globals.base')

@section('scripts')
    <script src= "{{ asset('/assets/scripts/dashboard.js') }}"></script>
@endsection

@section('contents')
	
	<div class="row">
		<div class="col-md-4">
			{{-- <div class="section-title m-top-30">
				<img src="assets/images/icons/chat-bubble.png">
				<div class="section-title-content">
					<h1>Mau ngapain bos?</h1>
					<p>Jangan lupa <i>check-in</i> dulu sebelum beraktivitas.</p>
				</div>
			</div> --}}
			<div class="inner-content m-top-5">
				<div class="card" id="check_in">
					<div class="card-header">
						<h5 class="card-title"><strong>Tap your card to check-in / out</strong></h5>
						<p class="card-category">Select the textbox below and use provided scanner to scan your ID. If the scanner doesn't work, enter token manually instead.</p>
					</div>
					<div class="card-body">
						<div class="input-group" id="frmInputToken">
							<input id="txtInputToken" type="password" class="form-control" placeholder="Select here and scan your ID" aria-label="Recipient's username" aria-describedby="basic-addon2">
							<div class="input-group-append">
								<button class="btn btn-success" type="button" onclick="validate()">Validate</button>
							</div>
						</div>
						
						<div id="frmValidation" style="display: none;">
							<div class="sk-circle">
								<div class="sk-circle1 sk-child"></div>
								<div class="sk-circle2 sk-child"></div>
								<div class="sk-circle3 sk-child"></div>
								<div class="sk-circle4 sk-child"></div>
								<div class="sk-circle5 sk-child"></div>
								<div class="sk-circle6 sk-child"></div>
								<div class="sk-circle7 sk-child"></div>
								<div class="sk-circle8 sk-child"></div>
								<div class="sk-circle9 sk-child"></div>
								<div class="sk-circle10 sk-child"></div>
								<div class="sk-circle11 sk-child"></div>
								<div class="sk-circle12 sk-child"></div>
							</div>
							<center><p>Please wait, validating your token...</p></center>
						</div>
						
						<div id="frmSuccess" style="display: none;">
							<center>
								<div class="svg-box">
									<svg class="circular green-stroke">
										<circle class="path" cx="75" cy="75" r="50" fill="none" stroke-width="5" stroke-miterlimit="10"/>
									</svg>
									<svg class="checkmark green-stroke">
										<g transform="matrix(0.79961,8.65821e-32,8.39584e-32,0.79961,-489.57,-205.679)">
											<path class="checkmark__check" fill="none" d="M616.306,283.025L634.087,300.805L673.361,261.53"/>
										</g>
									</svg>
								</div>	
								<p>Verification complete, enjoy your day!</p>
								<p><button class="btn btn-danger" onclick="reset()">Verify Another Account</button></p>
							</center>
						</div>

						<div id="frmTapout" style="display: none;">
							<center>
								<div class="svg-box">
									<svg class="circular yellow-stroke">
										<circle class="path" cx="75" cy="75" r="50" fill="none" stroke-width="5" stroke-miterlimit="10"/>
									</svg>
									<svg class="alert-sign yellow-stroke">
										<g transform="matrix(1,0,0,1,-615.516,-257.346)">
											<g transform="matrix(0.56541,-0.56541,0.56541,0.56541,93.7153,495.69)">
												<path class="line" d="M634.087,300.805L673.361,261.53" fill="none"/>
											</g>
											<g transform="matrix(2.27612,-2.46519e-32,0,2.27612,-792.339,-404.147)">
												<circle class="dot" cx="621.52" cy="316.126" r="1.318" />
											</g>
										</g>
									</svg>
								</div>	
								<p>You've signed in before, are you about to sign out?</p>
								<p>
									<a href="/evaluation/presence/validate" class="btn btn-danger">Logout Now</a>
									<button class="btn btn-default" onclick="reset()">Cancel</button>
								</p>
							</center>
						</div>

						<div id="frmFailed" style="display: none;">
							<center>
								<div class="svg-box">
									<svg class="circular red-stroke">
										<circle class="path" cx="75" cy="75" r="50" fill="none" stroke-width="5" stroke-miterlimit="10"/>
									</svg>
									<svg class="cross red-stroke">
										<g transform="matrix(0.79961,8.65821e-32,8.39584e-32,0.79961,-502.652,-204.518)">
											<path class="first-line" d="M634.087,300.805L673.361,261.53" fill="none"/>
										</g>
										<g transform="matrix(-1.28587e-16,-0.79961,0.79961,-1.28587e-16,-204.752,543.031)">
											<path class="second-line" d="M634.087,300.805L673.361,261.53"/>
										</g>
									</svg>
								</div>
								<p>You've entered invalid credentials</p>
								<p><a href="#" class="btn btn-danger" onclick="reset()">Try Again</a></p>
							</center>
						</div>
					</div>
				</div>
			</div>

			<div class="inner-content m-top-10">
				<div class="row rem-space">
					<div class="col-md-4 shortcut-container">
						<a href="/accounts/logout" class="shortcut-button">
							<img src="/assets/images/icons/login.png" />
							<p>Logout</p>
						</a>
					</div>
					<div class="col-md-4 shortcut-container">
						<a href="/pos/sales" class="shortcut-button">
							<img src="/assets/images/icons/budget.png" />
							<p>Point of Sales</p>
						</a>
					</div>
					<div class="col-md-4 shortcut-container">
						<a href="#" class="shortcut-button">
							<img src="/assets/images/icons/box-1.png" />
							<p>Warehouse</p>
						</a>
					</div>
					<div class="col-md-4 shortcut-container">
						<a href="#" class="shortcut-button">
							<img src="/assets/images/icons/note.png" />
							<p>Ledger</p>
						</a>
					</div>
				</div>
			</div>

			{{-- <div class="section-title m-top-30">
				<img src="assets/images/icons/megaphone.png">
				<div class="section-title-content">
					<h1>Pengumuman Penting</h1>
					<p>Disimak, dibaca dan dicermati ya gengs.</p>
				</div>
			</div> --}}
		</div>		
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-4 news-container">
					<a href="#" class="news-box" style="background-image: url(/assets/images/charles-deluvio-540420-unsplash.jpg)">
						<div class="news-overlay">
							<div class="news-content">
								<h5>Pengambilan UMN Hoodie Gen Lama <label class="badge badge-danger">PRIORITAS</span></h5>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 news-container">
					<a href="#" class="news-box" style="background-image: url(/assets/images/charles-deluvio-540420-unsplash.jpg)">
						<div class="news-overlay">
							<div class="news-content">
								<h5>Cara Order Barang PO untuk Maxima <label class="badge badge-warning">PENTING</span></h5>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 news-container">
					<a href="#" class="news-box" style="background-image: url(/assets/images/charles-deluvio-540420-unsplash.jpg)">
						<div class="news-overlay">
							<div class="news-content">
								<h5>Cara Absensi yang Baru Baca Disini <label class="badge badge-warning">PENTING</span></h5>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 news-container">
					<a href="#" class="news-box" style="background-image: url(/assets/images/charles-deluvio-540420-unsplash.jpg)">
						<div class="news-overlay">
							<div class="news-content">
								<h5>Perubahan Jadwal Jaga Semester 7 <label class="badge badge-success">DIPERHATIKAN</span></h5>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 news-container">
					<a href="#" class="news-box" style="background-image: url(/assets/images/charles-deluvio-540420-unsplash.jpg)">
						<div class="news-overlay">
							<div class="news-content">
								<h5>Jangan Lupa Denda & Uang Kas <label class="badge badge-success">DIPERHATIKAN</span></h5>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>

@endsection