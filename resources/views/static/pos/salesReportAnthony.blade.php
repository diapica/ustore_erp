<!DOCTYPE html>
<html>
<head>
	<title>Sales Report</title>
	<style>
	table, th, td {
	    border: 1px solid black;
	    border-collapse: collapse;
	}
	th, td {
	    padding: 5px;
	    text-align: left;    
	}

	ul{
		padding: 0;
	}
	ul li{
		margin-right: 20px;
		display: inline;
	}
	</style>
</head>
<body>
	<form id="period" action="" method="GET">
		<!-- <input type="month" name="startDate">
		<input type="month" name="endDate">
		<button>Search</button> -->
		<select name="timeUnitOption" onchange="giveTimeUnit(this.value)">
			<option>Select Time Unit</option>
			<option>Year</option>
			<option>Month</option>
			<option>Day</option>
		</select>

		<div id="dynamicFormContent" style="margin-top: 10px;">
			
		</div>

		
	</form>
	<hr>
	<table>
	    <thead>
	      <tr>
	      	<th rowspan="2">Category</th>
	        <th rowspan="2">Product Name</th>
	        <!-- @if(count($sumPerPeriod))
	        	@foreach($sumPerPeriod as $sumPerPeriodEach)
	        		<th>{{$sumPerPeriodEach->month}}, {{$sumPerPeriodEach->year}}</th>
	        	@endforeach
	        @endif -->
	        @if(!empty($timeUnit))
	        	@php
	        		$unit = array_shift($timeUnit);
	        	@endphp
	        	@if($unit == 'month')
	        		@foreach($timeUnit as $timeUnitEach)
	        			<th colspan="2">{{ date('F',strtotime($timeUnitEach)) }}, {{ date('Y',strtotime($timeUnitEach)) }}</th>
	        		@endforeach

	        	@elseif($unit == 'day')
	        		@foreach($timeUnit as $timeUnitEach)
	        			<th colspan="2">{{ date('d',strtotime($timeUnitEach)) }}, {{ date('M',strtotime($timeUnitEach)) }} {{ date('Y',strtotime($timeUnitEach)) }}</th>
	        		@endforeach

	        	@elseif($unit == 'year')
	        		@foreach($timeUnit as $timeUnitEach)
	        			<th colspan="2">{{ $timeUnitEach }}</th>
	        		@endforeach

	        	@endif
	        	
	        @endif
	      </tr>

	      <tr>
	      	@if(!empty($timeUnit))
	      		@foreach($timeUnit as $timeUnitEach)
	      			<th>Net</th>
	      			<th>Qty</th>
	      		@endforeach
	      	@endif
	      </tr>

	    </thead>
	    @if(count($sumPerPeriod) && count($sumPerCategory) && count($sumPerProduct))
	    	@if(count($soldCategoryArr))
	    		@foreach($soldCategoryArr as $soldCategoryArrEach)
	    			<tr>
	    				<td>{{$soldCategoryArrEach->category}}</td>

	    				
	    				
	    			</tr>

	    			<!-- @foreach($soldCategoryArrEach->product as $product)
	    				<tr>
	    					<td></td>
	    					<td>{{$product->product_name}}</td>
	    					@foreach($sumPerProduct as $sumPerProductEach)
	    						@if($sumPerProductEach->product_name === $product->product_name)
	    							<td>{{$sumPerProductEach->net}}</td>
	    						@endif
	    					@endforeach
	    				</tr>
	    			@endforeach
	    			
	    			<tr>
	    				<td></td>
	    				<td>Total Per Category</td>
	    				@foreach($sumPerCategory as $sumPersoldCategoryArrEach)
	    					@if($sumPersoldCategoryArrEach->category === $soldCategoryArrEach->category)
	    						<td>{{$sumPersoldCategoryArrEach->net}}</td>
	    					@endif
	    				@endforeach
	    			</tr> -->

	    			@foreach($soldProductArr->where('category_id',$soldCategoryArrEach->id) as $product)
	    				<tr>
	    					<td></td>
	    					<td>{{$product->product_name}}</td>

	    					@foreach($timeUnit as $timeUnitEach)
	    						@if(count($sumPerProduct->where('product_name',$product->product_name)->where('time_unit',$timeUnitEach)))
	    							<td>Rp.{{number_format($sumPerProduct->where('product_name',$product->product_name)->where('time_unit',$timeUnitEach)->first()->net)}} 
	    							</td>

	    							<td>{{ number_format ( $sumPerProduct->where('product_name',$product->product_name)->where('time_unit',$timeUnitEach)->first()->total_qty) }} PCS</td>
	    							
	    						@else
	    							<td colspan="2"></td>
	    						@endif
	    					@endforeach

	    					<!-- @if(count($sumPerProduct->where('product_name',$product->product_name)->where('month','November')))
	    						<td>{{$sumPerProduct->where('product_name',$product->product_name)->where('month','November')->first()->net}}</td>
	    					@else
	    						<td></td>
	    					@endif

	    					@if(count($sumPerProduct->where('product_name',$product->product_name)->where('month','January')))
	    						<td>{{$sumPerProduct->where('product_name',$product->product_name)->where('month','January')->first()->net}}</td>
	    					@else
	    						<td></td>
	    					@endif

	    					@if(count($sumPerProduct->where('product_name',$product->product_name)->where('month','February')))
	    						<td>{{$sumPerProduct->where('product_name',$product->product_name)->where('month','February')->first()->net}}</td>
	    					@else
	    						<td></td>
	    					@endif -->
	    				</tr>
	    			@endforeach

	    			<tr>
	    				<td></td>
	    				<td></td>
	    			</tr>

	    			<tr>
	    				<td></td>
	    				<td>Total Per Category</td>

	    				@foreach($timeUnit as $timeUnitEach)
	    					@if(count($sumPerCategory->where('category',$soldCategoryArrEach->category)->where('time_unit',$timeUnitEach)))
	    						<td style="background-color: #33D1FF;">Rp.{{number_format($sumPerCategory->where('category',$soldCategoryArrEach->category)->where('time_unit',$timeUnitEach)->first()->net)}}</td>

	    						<td style="background-color: #33D1FF;">{{number_format($sumPerCategory->where('category',$soldCategoryArrEach->category)->where('time_unit',$timeUnitEach)->first()->total_qty)}} PCS</td>
	    					@else
	    						<td colspan="2"></td>
	    					@endif
	    				@endforeach

	    				<!-- @if(count($sumPerCategory->where('category',$soldCategoryArrEach->category)->where('month','November')))
	    					<td>{{$sumPerCategory->where('category',$soldCategoryArrEach->category)->where('month','November')->first()->net}}</td>
	    				@else
	    					<td></td>
	    				@endif

	    				@if(count($sumPerCategory->where('category',$soldCategoryArrEach->category)->where('month','January')))
	    					<td>{{$sumPerCategory->where('category',$soldCategoryArrEach->category)->where('month','January')->first()->net}}</td>
	    				@else
	    					<td></td>
	    				@endif

	    				@if(count($sumPerCategory->where('category',$soldCategoryArrEach->category)->where('month','February')))
	    					<td>{{$sumPerCategory->where('category',$soldCategoryArrEach->category)->where('month','February')->first()->net}}</td>
	    				@else
	    					<td></td>
	    				@endif -->
	    			</tr>

	    		@endforeach

	    		<tr>
	    			<td></td>
	    			<td></td>
	    		</tr>
	    		
	    		<tr>
	    			<td></td>
	    			<td style="background-color:#33D1FF;">Grand Total</td>
	    			@foreach($sumPerPeriod as $sumPerPeriodEach)
	    				<td style="background-color:#33D1FF;">Rp.{{number_format($sumPerPeriodEach->net - $sumDiscPerPeriod->where('time_unit',$sumPerPeriodEach->time_unit)->first()->total_disc)}}</td>

	    				<td style="background-color:#33D1FF;">{{number_format($sumPerPeriodEach->total_qty)}} PCS</td>
	    			@endforeach
	    		</tr>
	    	@endif
	    @else
	    	<tr>
	    		<td colspan="2" style="text-align: center;">No Record Found</td>
	    	</tr>
	    @endif
	    
	</table>
</body>

<script src=" {{ asset('/assets/js/core/jquery.3.2.1.min.js') }} " type="text/javascript"></script> 
<script type="text/javascript">


	function giveTimeUnit(timeUnit){
		if(timeUnit == 'Year'){
			$("#dynamicFormContent").empty();
		}
		else if( timeUnit == 'Month'){
			$("#dynamicFormContent").empty();
			$("#dynamicFormContent").append('<div> Start Month : <input type="month" name="startDate"></div>');
			$("#dynamicFormContent").append('<div> End Month : <input type="month" name="endDate"></div>');
			$("#dynamicFormContent").append('<div><button>Search</button></div>');
		}
		else if( timeUnit == 'Day'){
			$("#dynamicFormContent").empty();
			$("#dynamicFormContent").append('<div> Start Date : <input type="date" name="startDate"></div>');
			$("#dynamicFormContent").append('<div> End Date : <input type="date" name="endDate"></div>');
			$("#dynamicFormContent").append('<div><button>Search</button></div>');

			$("#dynamicFormContent").append("<ul><li><a href='/pos/sales/report?timeUnitOption=Day&startDate={{$periodOption['currentDay']['startDate']}}&endDate={{$periodOption['currentDay']['endDate']}}'>|Current Day|</a></li>						<li><a href='/pos/sales/report?timeUnitOption=Day&startDate={{$periodOption['currentMonth']['startDate']}}&endDate={{$periodOption['currentMonth']['endDate']}}'>|Current Month|</a></li>						<li><a href='/pos/sales/report?timeUnitOption=Day&startDate={{$periodOption['yesterday']['startDate']}}&endDate={{$periodOption['yesterday']['endDate']}}'>|Yesterday|</a></li>						<li><a href='/pos/sales/report?timeUnitOption=Day&startDate={{$periodOption['last7Days']['startDate']}}&endDate={{$periodOption['last7Days']['endDate']}}'>|Last 7 Days|</a></li>						<li><a href='/pos/sales/report?timeUnitOption=Day&startDate={{$periodOption['last30Days']['startDate']}}&endDate={{$periodOption['last30Days']['endDate']}}'>|Last 30 Days|</a></li>				</ul>					"

			);
		}
		else{
			$("#dynamicFormContent").empty();
		}
	}

	
</script>

</html>