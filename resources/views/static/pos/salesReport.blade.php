Date : {{date('d F y')}}
		<table  border=1px>
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Produk</th>
					<th>Jumlah</th>
					<th>Harga Satuan</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				@foreach($data_purchase_detail as $key => $data)
				<tr>
					<td>{{ $key+1 }}</td>
					<td>{{ $data->product['product_name'] }}</td>
					<td>{{ $data['qty'] }}</td>
					<td>Rp {{ number_format($data['price'],'0',',','.') }}</td>
					<td>Rp {{ number_format($data['total'],'0',',','.') }}</td>
				</tr>
				@endforeach
				<tr>
					<td colspan="4">Subtotal :</td>
					<td colspan="2">Rp {{ number_format($data->Purchase['subtotal'],'0',',','.') }}</td>
				</tr>
				<tr>
					<td colspan="4">Disc :</td>
					<td colspan="2">Rp {{ number_format($data->Purchase['disc'],'0',',','.') }}</td>
				</tr>
				<tr>
					<td colspan="4">Tax :</td>
					<td colspan="2">Rp {{ number_format($data->Purchase['tax'],'0',',','.') }}</td>
				</tr>
				<tr>
					<td colspan="4">GrandTotal :</td>
					<td colspan="2">Rp {{ number_format($data->Purchase['gtotal'],'0',',','.') }}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<a href=" {{ url('/pos/sales') }} "><button>Back</button></a>
