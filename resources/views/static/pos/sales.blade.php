@extends('globals.base')

@section('scripts')

<script src= "{{ asset('/assets/scripts/pos.js') }}"></script>

@endsection

@section('contents')

<div class="wizzard row" id="pos-wizzard">
    <div class="col-md-4 wizzard-bg" style="background:url(/assets/images/rawpixel-670711-unsplash.jpg)">
        <div class="wizzard-overlay">
            <div class="overlay-content">
                {{-- <h1>Point of<br/>Sales</h1> --}}
            </div>
        </div>
    </div>
    <div class="col-md-8 wizzard-content">
        <div class="wizzard-step">
            <div id="step1" class="wizzard-step-content">
                <h3>Hi Jeremias Rama Wijaya, welcome to <strong>Point of Sales</strong></h3>
                <p class="m-top-30">We need to find you your customer first before you continue:</p>
                <div class="input-group m-top-30" id="frmSearchCustomer">
                    <input id="txtSearchCustomer" type="text" class="form-control" placeholder="Enter customer ID" aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-success" type="button" onclick="findCustomer()">Search Customer</button>
                    </div>
                </div>

                <div id="findingCustomer" style="display:none;">
                    <h3><strong>Validating...</strong></h3>

                    <div class="sk-circle">
                        <div class="sk-circle1 sk-child"></div>
                        <div class="sk-circle2 sk-child"></div>
                        <div class="sk-circle3 sk-child"></div>
                        <div class="sk-circle4 sk-child"></div>
                        <div class="sk-circle5 sk-child"></div>
                        <div class="sk-circle6 sk-child"></div>
                        <div class="sk-circle7 sk-child"></div>
                        <div class="sk-circle8 sk-child"></div>
                        <div class="sk-circle9 sk-child"></div>
                        <div class="sk-circle10 sk-child"></div>
                        <div class="sk-circle11 sk-child"></div>
                        <div class="sk-circle12 sk-child"></div>
                    </div>
                </div>
            </div>

            <div id="step2" class="wizzard-step-content text-center">
                <h3><strong>Finding Customer</strong></h3>
                
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
            </div>

            <div id="step3" class="wizzard-step-content">
                <h3>Customer Registration</h3>
                <p class="m-top-30">It seems that this customer <strong>haven't been registered</strong> in our database yet. Please kindly ask this following information:</p>
                <div class="row m-top-30">
                    <div class="form-group col-lg-8 m-bottom-5">
                        <label>Full Name</label>
                        <input type="text" name="nama" id="input_nama" class="form-control">
                    </div>
                    <div class="form-group col-lg-4 m-bottom-5">
                        <label>Generation</label>
                        <input type="number" name="angkatan" id="input_angkatan" class="form-control">
                        </div>
                    <div class="form-group col-lg-12 m-bottom-5">
                        <label>Email @student.umn.ac.id</label>
                        <input type="email" name="email" id="input_email" class="form-control">
                    </div>
                    <div class="form-group col-lg-6 m-bottom-5">
                        <label>Faculty</label>
                        <select class="form-control" name="fakultas" id="input_fakultas">
                            <option>Fakultas Teknik dan Informatika</option>
                            <option>Fakultas Bisnis</option>
                            <option>Fakultas Ilmu Komunikasi</option>
                            <option>Fakultas Seni dan Desain</option>
                            <option>D3 Perhotelan</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-6 m-bottom-5">
                        <label>Majors</label>
                        <input type="text" name="jurusan" id="input_jurusan" class="form-control">
                    </div>
                </div>
                <p>
                    <button class="btn btn-default" onclick="exit()">Exit</button>
                    <button class="btn btn-default" onclick="cancel()">Search Again</button>
                    <button class="btn btn-primary">Register Customer</button>
                </p>
            </div>

            <div id="step4" class="wizzard-step-content text-center">
                
                <div class="avatar-container">
                    <div class="avatar avatar-large m-auto" style="background: url(/assets/images/default_user.jpg)"></div>
                    <h5><strong id="lblCustName">Jeremias Rama Wijaya</strong></h5>
                    <p class="m-top-0"><span id="lblCollegeInfo">Teknik Informatika, Informatika 2015</span></p>
                    <p class="m-top-10">
                        <img src="/assets/images/icons/precious-stone.png" class="game-icon" />
                        <span id="lblCustPoints">200 points</span>
                    </p>
                </div>

                {{-- <p><strong>Last purchase </strong>on 23rd April 2018</p> --}}

                <p class="m-top-30">
                    <button class="btn btn-default" onclick="exit()">Exit</button>
                    <button class="btn btn-default" onclick="cancel()">Change Customer</button>
                    <button class="btn btn-primary" onclick="sales()">Proceed to Sales</button>
                </p>
            </div>

        </div>
    </div>
</div>

<div class="dashboard-container row" id="pos-main" style="display: none;">
    <div class="col-md-6">
        
        {{-- Shortcut --}}
        <div class="row">
            <div class="col-md-3 shortcut-container">
                <a href="/accounts/logout" class="shortcut-button">
                    <img src="/assets/images/icons/coupon.png" />
                    <p>Gift Card & Disc</p>
                </a>
            </div>
            <div class="col-md-3 shortcut-container">
                <a href="/accounts/logout" class="shortcut-button">
                    <img src="/assets/images/icons/file.png" />
                    <p>Take Note</p>
                </a>
            </div>
            <div class="col-md-3 shortcut-container">
                <a href="/accounts/logout" class="shortcut-button">
                    <img src="/assets/images/icons/sharing.png" />
                    <p>Change Customer</p>
                </a>
            </div>
            <div class="col-md-3 shortcut-container">
                <a href="/accounts/logout" class="shortcut-button">
                    <img src="/assets/images/icons/clothing.png" />
                    <p>UMN Hoodie</p>
                </a>
            </div>
            <div class="col-md-3 shortcut-container">
                <a href="/accounts/logout" class="shortcut-button">
                    <img src="/assets/images/icons/clothing.png" />
                    <p>Lanyard Batik</p>
                </a>
            </div>
            <div class="col-md-3 shortcut-container">
                <a href="/accounts/logout" class="shortcut-button">
                    <img src="/assets/images/icons/clothing.png" />
                    <p>Lanyard Monster</p>
                </a>
            </div>
            <div class="col-md-3 shortcut-container">
                <a href="/accounts/logout" class="shortcut-button">
                    <img src="/assets/images/icons/clothing.png" />
                    <p>Lanyard HIMTI</p>
                </a>
            </div>
        </div>

        {{-- Search Product --}}
        <div class="row m-top-30">
            <div class="col-md-6">
                <label class="control-label">Search Product</label>
                <input type="text" id="search-item" class="search form-control" placeholder="Find Item">
            </div>
            <div class="col-md-6">
                <label class="control-label">Scan barcode</label>
                <input type="text" id="barcode-item" class="barcode form-control" placeholder="Scan Barcode Here">
            </div>
        </div>

        {{-- Product Table --}}
        <div class="m-top-30" id="search_result">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <thead>
                    <tr>
                    <th>No</th>
                    <th>Nama Produk</th>
                    <th>Stock</th>
                    <th>Harga</th>
                    <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($products_list)==0)
                    <tr>
                    <td colspan="5"><center>There's No Product in Database, Please insert the Products First </center></td>
                    
                    </tr>
                    @else
                        @foreach($products_list as $key => $product)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $product['product_name'] }}</td>
                            <td>{{ $product['stock'] }}</td>
                            <td>Rp. {{ number_format($product['price'],0,',','.') }}</td>
                            <td>
                            <button type="button" class="btn btn-info btn-verysmall" onclick="addCart({{ $product['id'] }})">
                                <span class="btn-label">
                                <i class="fal fa-cart-plus"></i>
                                </span>
                                Add To Cart
                            </button>
                            </td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>
    </div>
</div>

@endsection