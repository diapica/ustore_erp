@extends('globals.base')

@section('contents')

<!-- begin content -->
                    <div class="row">
                        <!-- begin sales table -->
                        <div class="col-md-12">
                            <div class="card data-tables">
                                <div class="card-header">
                                    <h4 class="card-title">Sales History</h4>
                                    <p class="card-category">Point Of Sales</p>
                                </div>
                                <div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
                                    <div class="toolbar">
                                    <div class="container-fluid">
                                    <form action="" method="GET">
                                            <div class="row px-0">
                                                <div class="col-md-3 text-right">
                                                <label class="control-label mx-auto">Time Range : </label>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                 <input id="datetimepicker" name="startDate" type="text" class="form-control form-control-sm datepicker " placeholder="From Date">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                 <input name="endDate" type="text" class="form-control form-control-sm datepicker" placeholder="To Date">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                 <button type="submit" class="btn btn-info btn-fill pull-right">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                    </div>
                                    <div class="fresh-datatables">
                                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Date / Time</th>
                                                    <th>Invoice ID</th>
                                                    <th>Staff Name</th>
                                                    <th>Cust. Name</th>
                                                    <th>Sub-Total</th>
                                                    <th>Discount</th>
                                                    <th>Total</th>
                                                    <th class="disabled-sorting text-right">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($sales) > 0 )
	    	                                    @foreach($sales as $sale)
                                                	    @php
	    			                                        $sumPerPurchase = 0;
	    		                                        @endphp
                                                <tr>
                                                    <td>{{ $sale->created_at->toFormattedDateString() }}</td>
                                                    <td>{{$sale->id}}</td>
                                                    <td>{{$sale->staff->name}}</td>
                                                    <td>{{$sale->customer->name}}</td>
                                                    @foreach($sale->purchaseDetail as $purchaseDetailEach)
	    			                                	@php
	    				                                	$sumPerPurchase += $purchaseDetailEach->product->price * $purchaseDetailEach->qty;
	    				                                @endphp
                                                    @endforeach
                                                    @php 
                                                        $subtotal = $sumPerPurchase - $sale->disc;
                                                    @endphp
                                                    <td>Rp.{{number_format($sumPerPurchase)}}</td>
                                                    <td>Rp. -{{number_format($sale->disc)}}</td>
                                                    <td>Rp.{{number_format($sumPerPurchase - $sale->disc)}}</td>
                                                    <td class="text-right">
                                                        <button type="button" class="btn btn-outline btn-default" data-toggle="modal" data-target="#detailsModal-{{ $sale->id }}">
                                                            <span class="btn-label">
                                                                <i class="far fa-info"></i>
                                                            </span>
                                                            Details
                                                        </button>
                                                    </td>
                                                </tr>

                    <!-- Begin Details Modal -->
                    <div class="modal fade" id="detailsModal-{{ $sale->id }}" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="detailsModalLabel">Transaction Details</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">
                                            <i class="fal fa-times"></i>
                                        </span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <fieldset>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-4 control-label">Date / Time</label>
                                                <div class="col-sm-8">
                                                    <p class="form-control-static">{{ $sale->created_at }}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-4 control-label">Invoice ID</label>
                                                <div class="col-sm-8">
                                                    <p class="form-control-static">{{$sale->id}}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-4 control-label">Staff Name</label>
                                                <div class="col-sm-8">
                                                    <p class="form-control-static">{{$sale->staff->name}}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-4 control-label">Cust. Name</label>
                                                <div class="col-sm-8">
                                                    <p class="form-control-static">{{$sale->customer->name}}</p>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                            <label class="col-sm-12 text-left control-label">Product List</label>
                                            </div>
                                        @foreach($sale->purchaseDetail as $purchaseDetailEach)
	    			                    	@php
	    				                    	$sumPerPurchase += $purchaseDetailEach->product->price * $purchaseDetailEach->qty;
	    				                    @endphp
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <p class="form-control-static">{{$purchaseDetailEach->product->product_name}}</p>
                                                </div>
                                                <div class="col-sm-2">
                                                    <p class="form-control-static">{{$purchaseDetailEach->qty}}x</p>
                                                </div>
                                                <div class="col-sm-3">
                                                    <p class="form-control-static">Rp.{{number_format($purchaseDetailEach->product->price)}}</p>
                                                </div>
                                                <div class="col-sm-3">
                                                    <p class="form-control-static">Rp.{{number_format($purchaseDetailEach->product->price * $purchaseDetailEach->qty)}}</p>
                                                </div>
                                            </div>
                                        @endforeach
                                        <hr>
                                            <div class="row">
                                                <label class="col-sm-4 control-label">Sub-Total</label>
                                                <div class="col-sm-8">
                                                    <p class="form-control-static">Rp.{{number_format($sumPerPurchase)}}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-4 control-label">Discount</label>
                                                <div class="col-sm-8">
                                                    <p class="form-control-static">Rp. -{{number_format($sale->disc)}}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-4 control-label">Total</label>
                                                <div class="col-sm-8">
                                                    <p class="form-control-static">Rp.{{number_format($sumPerPurchase - $sale->disc)}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger" onclick="revokeTransaction();">
                                                            <span class="btn-label">
                                                                <i class="far fa-trash-alt"></i>
                                                            </span>
                                                            Revoke
                                                        </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Details Modal -->
                                                @endforeach
                                            @else
                                            <tr>
                                                <td colspan="8" style="text-align: center;width:100% "> There's nothing important at the moment</td>
                                            <tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Sales Table -->
                    </div>
                    <!-- End content -->
@endsection

@section('scripts')
<script>
    $(document).ready(function () {
        $('#datatables').dataTable();
    });
</script>
<script>
    $().ready(function() {


    if ($("#datetimepicker").length != 0) {

        $('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });

    };

});
</script>
<script>
    function revokeTransaction() {
        swal({
            type: 'warning',
            title: 'Are you sure ?',
            text: 'Confirm your password to continue',
            showCancelButton: true,
            confirmButtonColor: '#FB404B',
            confirmButtonText: 'Yes, delete it!',
            inputAttributes: {
                'autocapitalize': 'off',
                'autocorrect': 'off'
            }
        }).then((result) => {
            if (result.value) {
                swal(
                    'Deleted!',
                    'Transaction has been deleted.',
                    'success'
                )
            }
        })
    }

</script>
@endsection