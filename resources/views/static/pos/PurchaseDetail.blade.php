
<div class="card side-cart">
    <div class="card-header">
        <h4 class="card-title">Transaction Detail</h4>
    </div>

    <div class="card-body">
        @if(count($temp_purchase_detail)==0)
            <div class="row cart-item">
                <div class="col-12 text-center f18">
                    <label>There's Nothing Here</label>
                </div>
            </div>
        @else
            @foreach($temp_purchase_detail as $temp)
            <div class="row cart-item">
                <div class="col-4">
                    <p class="cart-item-text">{{ $temp->Product['product_name']}}</p>
                    <p class="cart-item-text text-muted">
                        <b>{{ $temp['qty'] }}x</b> - Rp {{ number_format($temp['price'],0,',','.') }}
                    </p>
                </div>
                <div class="col-3">
                    <p class="cart-item-text">Rp {{ number_format($temp['qty']*$temp['price'],0,',','.') }}</p>
                </div>
                <div class="col-3">
                    <a  class="btn btn-primary btn-verysmall cart-remove" onclick="changeItemSum('plus','{{ $temp['id'] }}')">
                        <i class="fal fa-plus"></i>
                    </a>
                    <a  class="btn btn-primary btn-verysmall cart-remove" onclick="changeItemSum('minus','{{ $temp['id'] }}')">
                        <i class="fal fa-minus"></i>
                    </a>
                    <a  class="btn btn-primary btn-verysmall cart-remove" onclick="changeItemSum('delete','{{ $temp['id'] }}')">
                        <i class="fal fa-trash-alt"></i>
                    </a>
                </div>
            </div>
            <!-- {{$subtotal = $subtotal + $temp['qty']*$temp['price']}} -->
            @endforeach
        @endif
    </div>
    <div class="card-footer">        
        @if(count($temp_purchase_detail)!=0)
            <div class="form-group">
                <p class="m-all-0 greyed">Specific Discount</p>
                <input type="barcode" placeholder="Discount" class="form-control discount" onkeyup="discount()" value=0>
            </div>
            <hr />
            <div class=row>
                <div class="col-4">
                    <p class="m-all-0 greyed">Sub-Total</p>
                    <h4 class="m-all-0 subtotal" subtotal="{{ $subtotal }}">Rp. {{ number_format($subtotal,'0',',','.') }}</h4>
                </div>
                <div class="col-4">
                    <p class="m-all-0 greyed">Discount</p>
                    <h4 class="m-all-0 disc" subtotal="{{ $subtotal }}">Rp. 0</h4>
                </div>
                <!-- <div class="col-3">
                    <p class="m-all-0 greyed">Tax</p>
                    <h4 class="m-all-0">Rp. 0</h4>
                </div> -->
                <div class="col-4">
                    <p class="m-all-0 greyed">Grandtotal</p>
                    <h4 class="m-all-0 grandtotal">Rp. 0</h4>
                </div>
            </div>
            <hr />
            <div class="row m-top-30">
                <div class="col-md-12">
                    <button type="button" class="btn btn-block btn-success" onclick="proses()"><strong>Process Order: </strong><span class="grandtotal">Rp. {{ number_format($subtotal,'0',',','.') }}</span></button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    or <a href="#" onclick="clearCart( {{ $temp['temp_purchase_id']}} )">Clear cart items</a>
                </div>
            </div>
        @endif
    </div>
</div>