<!DOCTYPE html>
<html>
<head>
	<title>Sales History</title>
	<style>
	table, th, td {
	    border: 1px solid black;
	    border-collapse: collapse;
	}
	th, td {
	    padding: 5px;
	    text-align: left;    
	}
	ul{
		padding: 0;
	}
	ul li{
		margin-right: 20px;
		display: inline;
	}
	</style>
</head>
<body>
	<form action="" method="GET">
		<input type="date" name="startDate">
		<input type="date" name="endDate">
		<button>Search</button>
	</form>
	<ul> 
		<li><a href="/pos/sales/history?startDate={{$periodOption['currentDay']['startDate']}}&endDate={{$periodOption['currentDay']['endDate']}}">|Current Day|</a></li>

		<li><a href="/pos/sales/history?startDate={{$periodOption['currentMonth']['startDate']}}&endDate={{$periodOption['currentMonth']['endDate']}}">|Current Month|</a></li>

		<li><a href="/pos/sales/history?startDate={{$periodOption['yesterday']['startDate']}}&endDate={{$periodOption['yesterday']['endDate']}}">|Yesterday|</a></li>

		<li><a href="/pos/sales/history?startDate={{$periodOption['last7Days']['startDate']}}&endDate={{$periodOption['last7Days']['endDate']}}">|Last 7 Days|</a></li>

		<li><a href="/pos/sales/history?startDate={{$periodOption['last30Days']['startDate']}}&endDate={{$periodOption['last30Days']['endDate']}}">|Last 30 Days|</a></li>
	</ul>
	<hr>
	<?php $grandtotal = 0 ?>
	<table>
	    <thead>
	      <tr>
	      	<th>Date</th>
	        <th>Sales ID</th>
	        <th>Staff Name </th>
	        <th>Customer Name</th>

	        <th>Product Name</th>
	        <th>Quantity</th>
	        <th>Price</th>
	        <th>Total</th>
	      </tr>
	    </thead>

	    @if(count($sales))
	    	@foreach($sales as $sale)
	    		@php
	    			$sumPerPurchase = 0;
	    		@endphp
	    		<tr>
	    			<td>{{$sale->created_at->toFormattedDateString()}}</td>
	    			<td>{{$sale->id}}</td>
	    			<td>{{$sale->staff->name}}</td>
	    			<td>{{$sale->customer->name}}</td>
	    			@foreach($sale->purchaseDetail as $purchaseDetailEach)
	    				@php
	    					$sumPerPurchase += $purchaseDetailEach->product->price * $purchaseDetailEach->qty;
	    				@endphp
	    				<tr>
	    					<td></td>
	    					<td></td>
	    					<td></td>
	    					<td></td>
	    					<td>{{$purchaseDetailEach->product->product_name}}</td>
	    					<td>{{$purchaseDetailEach->qty}}</td>
	    					<td>Rp.{{number_format($purchaseDetailEach->product->price)}}</td>
	    					<td>Rp.{{number_format($purchaseDetailEach->product->price * $purchaseDetailEach->qty)}}</td>
	    				</tr>
	    				
	    			@endforeach
	    			<tr>
	    				<td></td>
	    				<td></td>
	    				<td></td>
	    				<td></td>
	    				<td colspan="3">Discount</td>
	    				<td>Rp. -{{number_format($sale->disc)}}</td>
	    			</tr>

	    			<tr>
	    				<td></td>
	    				<td></td>
	    				<td></td>
	    				<td></td>
	    				<td colspan="3" style="background-color:#33D1FF;">Sub Total</td>
	    				<?php 
								$subtotal = $sumPerPurchase - $sale->disc;
						?>

	    				<td style="background-color:#33D1FF;">Rp.{{number_format($sumPerPurchase - $sale->disc)}}</td>
	    			</tr>
	    			
	    			
	    		</tr>
	    		
	    	@endforeach
	    		<tr>
    				<td style="background-color:#33D1FF;" colspan=4>Grand Total</td>
    				<td colspan=4 style="background-color:#33D1FF;font-size:20px">Rp. {{number_format($grandtotal)}}</td>
	    		</tr>

	    @else
	    	<tr>
	    		<td colspan="8" style="text-align: center;">No Record Found</td>
	    	</tr>
	    @endif
	</table>
</body>
</html>