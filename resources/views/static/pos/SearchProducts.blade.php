 <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
    <thead>
      <tr>
        <th>No</th>
        <th>Product Name</th>
        <th>Stock</th>
        <th>Harga</th>
        <th class="disabled-sorting text-right">Actions</th>
      </tr>
    </thead>
    <tbody>
      @if(count($products_list)==0)
      <tr>
        <td colspan="6"><center>There's No  Product in Database, Please insert the Products First </center></td>
        
      </tr>
      @else
        @foreach($products_list as $key => $product)
        <tr>
          <td>{{ $key+1 }}</td>
          <td>{{ $product['product_name'] }}</td>
          <td>{{ $product['stock'] }}</td>
          <td>Rp. {{ number_format($product['price'],0,',','.') }}</td>
          <td class="text-right">
            <button type="button" class="btn btn-info"  onclick="addCart({{ $product['id'] }})">
              <span class="btn-label">
                <i class="far fa-cart-plus"></i>
              </span>
              Add To Cart
            </button>
          </td>
        </tr>
        @endforeach
      @endif
    </tbody>
  </table>