@extends('globals.base')

@section('contents')
    <div class="row">
        <div class="col-md-12">
            <div class="card data-tables">
                <div class="card-header">
                    <h4 class="card-title">Presence Overview</h4>
                    <a class="btn btn-info float-right" href="{{ url('/evaluation/presence/validate') }}">
                        <span class="btn-label"><i class="far fa-calendar-check"></i></span>
                        &nbsp&nbspValidate Presence
                    </a>
                    <br>
                </div>
                    <hr class="m-all-0" />
                    <div class="fresh-datatables">
                        <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Day</th>
                                    <th>Date</th>
                                    <th>Check In Time</th>
                                    <th>Check Out Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($presences)==0)
                                    <tr>
                                        <td colspan='7' style="text-align: center;width:100% "> You haven't made any presence validation yet</td>
                                        <td></td>
                                    </tr>
                                @else
                                    @foreach($presences as $presence)
                                    <tr>
                                        <td>{{ date('l', strtotime($presence['tap_in'])) }}</td>
                                        <td>{{ date('d F Y', strtotime($presence['tap_in'])) }}</td>
                                        <td>{{ date('H:i:s', strtotime($presence['tap_in'])) }}</td>
                                        @if ($presence['tap_out'])
                                        <td>{{ date('H:i:s', strtotime($presence['tap_out'])) }}</td>
                                        @else
                                        <td>Pending</td>
                                        @endif
                                    </tr>
                                    @endforeach 
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection