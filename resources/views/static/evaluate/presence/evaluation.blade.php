@extends('globals.base')

@section('scripts')
    <script src= "{{ asset('/assets/scripts/evaluation.js') }}"></script>
@endsection

@section('contents')

    <div class="wizzard row">
        <div class="col-md-4 wizzard-bg" style="background:url(/assets/images/glenn-carstens-peters-190592-unsplash.jpg)">
            <div class="wizzard-overlay">
                <div class="overlay-content">
                    {{-- <h1>Ustore<br/>On Duty<br/>Evaluation</h1> --}}
                </div>
            </div>
        </div>
        <div class="col-md-8 wizzard-content">
            <div class="wizzard-step">
                <div id="step1" class="wizzard-step-content">
                    <h3>Hi Jeremias Rama Wijaya, welcome to <strong>Ustore On Duty Evaluation</strong></h3>
                    <p class="m-top-30"><strong>Ustore On Duty Evaluation</strong> is used to make sure that each staff finished their predefined task for the current shift. Those tasks are: keeping ustore clean, serve customer (if any) with good services, monitor customer behaviour and stock in each shift. So, if something were to happen, we can deduce the right decision for the good of Ustore.</p>
                    <p>Before you take your leave, please answer these following question. After the assessment is complete, you will then be allowed to log out and continue your awesome day!</p>
                    <div class="m-top-30">
                            <a href="/"><button class="btn btn-default">Resume Session</button></a>
                        <button class="btn btn-primary" onclick="nextStep(1)">Take Test</button>
                    </div>
                </div>

                <div id="step2" class="wizzard-step-content">
                    <center id="step2a">
                        <h3><strong>Tap in your card</strong></h3>
                        
                        <div class="svg-box">
                            <svg class="circular yellow-stroke">
                                <circle class="path" cx="75" cy="75" r="50" fill="none" stroke-width="5" stroke-miterlimit="10"/>
                            </svg>
                            <svg class="alert-sign yellow-stroke">
                                <g transform="matrix(1,0,0,1,-615.516,-257.346)">
                                    <g transform="matrix(0.56541,-0.56541,0.56541,0.56541,93.7153,495.69)">
                                        <path class="line" d="M634.087,300.805L673.361,261.53" fill="none"/>
                                    </g>
                                    <g transform="matrix(2.27612,-2.46519e-32,0,2.27612,-792.339,-404.147)">
                                        <circle class="dot" cx="621.52" cy="316.126" r="1.318" />
                                    </g>
                                </g>
                            </svg>
                        </div>	
                        <p>Please let us know who you are by scanning your organization card to provided scanner. If the scanner doesn't work, please enter the verification code manually. If you can't get your code, please ask our IT Staff.</p>

                        <div class="input-group m-top-30" id="frmInputToken">
                            <input id="txtInputToken" type="password" class="form-control" placeholder="Select here and scan your ID" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="button" onclick="validate()">Validate</button>
                            </div>
                        </div>
                    </center>

                    <center id="step2b" style="display: none;">
                        <h3><strong>You haven't tapped in</strong></h3>

                        <div class="svg-box">
                            <svg class="circular yellow-stroke">
                                <circle class="path" cx="75" cy="75" r="50" fill="none" stroke-width="5" stroke-miterlimit="10"/>
                            </svg>
                            <svg class="alert-sign yellow-stroke">
                                <g transform="matrix(1,0,0,1,-615.516,-257.346)">
                                    <g transform="matrix(0.56541,-0.56541,0.56541,0.56541,93.7153,495.69)">
                                        <path class="line" d="M634.087,300.805L673.361,261.53" fill="none"/>
                                    </g>
                                    <g transform="matrix(2.27612,-2.46519e-32,0,2.27612,-792.339,-404.147)">
                                        <circle class="dot" cx="621.52" cy="316.126" r="1.318" />
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <p>You are trying to access evaluation while your card haven't been used to sign in. Your presence will be recorded. If this isn't right, please tap out and contact IT administrator.</p>
                        <p>
                            <button class="btn btn-danger" type="button" onclick="reset()">Tap out</button>
                            <button class="btn btn-default" type="button" onclick="backToHome()">Back to Home</button>
                        </p>
                    </center>

                    <center id="step2c" style="display: none;">
                        <h3><strong>Invalid Credentials</strong></h3>

                        <div class="svg-box">
                            <svg class="circular red-stroke">
                                <circle class="path" cx="75" cy="75" r="50" fill="none" stroke-width="5" stroke-miterlimit="10"/>
                            </svg>
                            <svg class="cross red-stroke">
                                <g transform="matrix(0.79961,8.65821e-32,8.39584e-32,0.79961,-502.652,-204.518)">
                                    <path class="first-line" d="M634.087,300.805L673.361,261.53" fill="none"/>
                                </g>
                                <g transform="matrix(-1.28587e-16,-0.79961,0.79961,-1.28587e-16,-204.752,543.031)">
                                    <path class="second-line" d="M634.087,300.805L673.361,261.53"/>
                                </g>
                            </svg>
                        </div>
                        <p>Oops, it looks like you entered the wrong credentials. If the code doesn't work, please enter the verification code manually. If you can't get your code, please ask our IT Staff.</p>
                        <p>
                            <button class="btn btn-danger" type="button" onclick="reset()">Try Again</button>
                            <button class="btn btn-default" type="button" onclick="backToHome()">Back to Home</button>
                        </p>
                    </center>

                    <center id="step2d" style="display: none;">
                        <h3><strong>Your credentials have been verified</strong></h3>

                        <div class="svg-box">
                            <svg class="circular green-stroke">
                                <circle class="path" cx="75" cy="75" r="50" fill="none" stroke-width="5" stroke-miterlimit="10"/>
                            </svg>
                            <svg class="checkmark green-stroke">
                                <g transform="matrix(0.79961,8.65821e-32,8.39584e-32,0.79961,-489.57,-205.679)">
                                    <path class="checkmark__check" fill="none" d="M616.306,283.025L634.087,300.805L673.361,261.53"/>
                                </g>
                            </svg>
                        </div>	
                        <p>Horray, it's you! We'll now proceed to the next step.</p>
                    </center>

                    <center id="step2d" style="display: none;">
                        <h3><strong>Validating...</strong></h3>

                        <div class="sk-circle">
                            <div class="sk-circle1 sk-child"></div>
                            <div class="sk-circle2 sk-child"></div>
                            <div class="sk-circle3 sk-child"></div>
                            <div class="sk-circle4 sk-child"></div>
                            <div class="sk-circle5 sk-child"></div>
                            <div class="sk-circle6 sk-child"></div>
                            <div class="sk-circle7 sk-child"></div>
                            <div class="sk-circle8 sk-child"></div>
                            <div class="sk-circle9 sk-child"></div>
                            <div class="sk-circle10 sk-child"></div>
                            <div class="sk-circle11 sk-child"></div>
                            <div class="sk-circle12 sk-child"></div>
                        </div>
                    </center>
                </div>

                <div id="step3" class="wizzard-step-content">
                    
                    {{-- Question --}}
                    <h5>Are you coming late?</h5>   
                    <br/>
                    
                    {{-- Option --}}
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio1" name="cboQuestion2" type="radio" value="I'm not late."/>
                        <label for="radio1">No</label>
                    </div>
                    <div class="inputGroup">
                        <input id="radio2" onclick="allowNext()" name="cboQuestion2" type="radio" value="I'm 5 minutes late."/>
                        <label for="radio2">Yes, around 5 minute</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio3" name="cboQuestion2" type="radio" value="I'm 10 minutes late."/>
                        <label for="radio3">Yes, around 10 minute</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio4" name="cboQuestion2" type="radio" value="I'm 15 minutes late."/>
                        <label for="radio4">Yes, around 15 minute</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio5" name="cboQuestion2" type="radio" value="I'm 20 minutes late."/>
                        <label for="radio5">Yes, above 20 minute</label>
                    </div>
                </div>

                <div id="step4" class="wizzard-step-content">
                    
                    {{-- Question --}}
                    <h5>Why are you coming late?</h5>  
                    <br/> 
                    
                    {{-- Option --}}
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio6" name="cboQuestion3" type="radio" value="I'm late because I have to attend class."/>
                        <label for="radio6">I have to attend class</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio7" name="cboQuestion3" type="radio" value="I'm late because I woke up late."/>
                        <label for="radio7">I woke up late</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio8" name="cboQuestion3" type="radio" value="I'm late because I'm stuck in the traffic jam."/>
                        <label for="radio8">Stuck in traffic jam</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio9" name="cboQuestion3" type="radio" value="I'm late because I had an accident on the way / personal problems."/>
                        <label for="radio9">I had an accident / personal problems</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio10" name="cboQuestion3" type="radio" value="I'm late because another reason."/>
                        <label for="radio10">Others</label>
                    </div>
                </div>

                <div id="step5" class="wizzard-step-content">
                    
                    {{-- Question --}}
                    <h5>Have you notified your coordinator and supervisor regarding your delay?</h5>  
                    <br/> 
                    
                    {{-- Option --}}
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio11" name="cboQuestion4" type="radio" value="I have told my supervisor about my delay"/>
                        <label for="radio11">Yes, I have</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio12" name="cboQuestion4" type="radio" value="I haven't told my supervisor about my delay"/>
                        <label for="radio12">No, I haven't</label>
                    </div>
                </div>

                <div id="step6" class="wizzard-step-content">
                    
                    {{-- Question --}}
                    <h5>What is the approximate number of customers who came to visit the store during your shift?</h5>  
                    <br/> 
                    
                    {{-- Option --}}
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio13" name="cboQuestion5" type="radio" value="There's no customer during my shift."/>
                        <label for="radio13">There's no customer during my shift</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio14" name="cboQuestion5" type="radio" value="There are about 1-5 customers during my shift."/>
                        <label for="radio14">1-5 people</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio15" name="cboQuestion5" type="radio" value="There are about 6-10 customers during my shift."/>
                        <label for="radio15">6-10 people</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio16" name="cboQuestion5" type="radio" value="There are about 11-20 customers during my shift."/>
                        <label for="radio16">11-20 people</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio17" name="cboQuestion5" type="radio" value="There are more than 20 customers during my shift."/>
                        <label for="radio17">Above 20</label>
                    </div>
                </div>    

                <div id="step7" class="wizzard-step-content">
                    
                    {{-- Question --}}
                    <h5>Is there any question regarding "UMN Hoodie" from the previous generation?</h5>  
                    <br/> 
                    
                    {{-- Option --}}
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio18" name="cboQuestion6" type="radio" value="Customer asked questions regarding UMN Hoodie."/>
                        <label for="radio18">Yes, there is</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio19" name="cboQuestion6" type="radio" value="There isn't a single customer asked about UMN Hoodie."/>
                        <label for="radio19">No, there isn't</label>
                    </div>
                </div>

                <div id="step8" class="wizzard-step-content">
                    
                    {{-- Question --}}
                    <h5>Is there any customer that makes you uncomfortable or give you disrespectful behaviour?</h5>  
                    <br/> 
                    
                    {{-- Option --}}
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio20" name="cboQuestion7" type="radio" value="There are some customers that makes me uncomfortable and act disrespectful."/>
                        <label for="radio20">Yes, there is</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio21" name="cboQuestion7" type="radio" value="All the customers are nice."/>
                        <label for="radio21">No, there isn't</label>
                    </div>
                </div>

                <div id="step9" class="wizzard-step-content">
                    
                    {{-- Question --}}
                    <h5>Is there any customer that wants to purchase certain product, but the product itself is out of stock?</h5>  
                    <br/> 
                    
                    {{-- Option --}}
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio22" name="cboQuestion8" type="radio" value="Some items are out of stock and customers want it."/>
                        <label for="radio22">Yes, there is</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio23" name="cboQuestion8" type="radio" value="No customer asked for older / out of stock items."/>
                        <label for="radio23">No, there isn't</label>
                    </div>
                </div>

                <div id="step10" class="wizzard-step-content">
                    
                    {{-- Question --}}
                    <h5>Is there any customer that don't want their identity recorded?</h5>  
                    <br/> 
                    
                    {{-- Option --}}
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio24" name="cboQuestion9" type="radio" value="Some customers don't want their identity to be recorded."/>
                        <label for="radio24">Yes, because he/she don't want to</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio25" name="cboQuestion9" type="radio" value="Some customers reasoned that they're late and recording their name takes too long."/>
                        <label for="radio25">Yes, because they don't have time for that</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio26" name="cboQuestion9" type="radio" value="All customers allow their identity to be recorded."/>
                        <label for="radio26">No, there isn't</label>
                    </div>
                </div>

                <div id="step11" class="wizzard-step-content">
                    
                    {{-- Question --}}
                    <h5>What's the condition of the trash bin by the time you checked in?</h5>  
                    <br/> 
                    
                    {{-- Option --}}
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio27" name="cboQuestion10" type="radio" value="The trash can is full, dirty and stings when I arrived."/>
                        <label for="radio27">It's full, dirty and stings</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio28" name="cboQuestion10" type="radio" value="The trash can is full when I arrived."/>
                        <label for="radio28">It's just full</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio29" name="cboQuestion10" type="radio" value="The trash can is half / partly full when I arrived."/>
                        <label for="radio29">It's half / partly full</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio30" name="cboQuestion10" type="radio" value="The trash can is half / partly full / empty when I arrived but stings."/>
                        <label for="radio30">It's half / partly full / empty but stings</label>
                    </div>
                    <div class="inputGroup" onclick="allowNext()">
                        <input id="radio31" name="cboQuestion10" type="radio" value="The trash can is empty and clean when I arrived."/>
                        <label for="radio31">It's empty / clean</label>
                    </div>
                </div>

                <div id="step12" class="wizzard-step-content text-center">
                    
                    <div id="evaluationValidating">
                        <h3><strong>Validating...</strong></h3>

                        <div class="sk-circle">
                            <div class="sk-circle1 sk-child"></div>
                            <div class="sk-circle2 sk-child"></div>
                            <div class="sk-circle3 sk-child"></div>
                            <div class="sk-circle4 sk-child"></div>
                            <div class="sk-circle5 sk-child"></div>
                            <div class="sk-circle6 sk-child"></div>
                            <div class="sk-circle7 sk-child"></div>
                            <div class="sk-circle8 sk-child"></div>
                            <div class="sk-circle9 sk-child"></div>
                            <div class="sk-circle10 sk-child"></div>
                            <div class="sk-circle11 sk-child"></div>
                            <div class="sk-circle12 sk-child"></div>
                        </div>
                    </div>

                    <div id="evaluationSubmitted" style="display: none;">
                        <h5>You have finished your evaluation</h5>  
                        <p>Once again, don't forget to take out the trash to keep our store clean. We thank you for helping Ustore into a better place, see you next week!</p>
                        <br/>

                        <div class="svg-box">
                            <svg class="circular green-stroke">
                                <circle class="path" cx="75" cy="75" r="50" fill="none" stroke-width="5" stroke-miterlimit="10"/>
                            </svg>
                            <svg class="checkmark green-stroke">
                                <g transform="matrix(0.79961,8.65821e-32,8.39584e-32,0.79961,-489.57,-205.679)">
                                    <path class="checkmark__check" fill="none" d="M616.306,283.025L634.087,300.805L673.361,261.53"/>
                                </g>
                            </svg>
                        </div>	

                        <div class="inputGroup">
                            <input id="radio32" name="cboQuestion11" type="checkbox"/>
                            <label for="radio32" id="cboLogout()" onclick="allowLogout()">I have clean the trash</label>
                        </div>
                    </div>

                    {{-- Button --}}
                    <div class="m-top-30">
                        <button class="btn btn-primary" id="btnLogout" onclick="logout()" disabled>Logout Now</button>
                    </div>
                </div>
            
                {{-- Button --}}
                <div class="m-top-30" id="navigation-button" style="display: none;">
                    {{-- <button class="btn btn-default" onclick="nextStep(-1)">Previous</button> --}}
                    <button class="btn btn-primary btn-long" id="btnNext" onclick="nextStep(1)">Next</button>
                </div>
            </div>

        </div>
    </div>

@endsection