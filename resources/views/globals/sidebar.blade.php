<?php $group_id =  Auth::user()->id_group?>
<!-- Begin Sidebar -->
<div class="sidebar" data-color="azure">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="{{ url('/')}}" class="simple-text logo-mini">U</a>
            <a href="{{ url('/')}}" class="simple-text logo-normal">U-Store ERP</a>
        </div>
        <div class="user">
            <div class="photo">
                <img src="http://via.placeholder.com/400/fafafa?text=IMG">
            </div>
            <div class="info ">
                <a data-toggle="collapse" href="#myprofile" class="collapsed" aria-expanded="false">
                    <span>Hi, <?php echo Auth::User()->name; ?>
                        <!-- <b class="caret"></b> -->
                    </span>
                </a>
            </div>
        </div>
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="/">
                <i class="fal fa-home"></i>
                <p>Dashboard</p>
                </a>
            </li>

            {{--  Menu: Human Resource  --}}
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#nav-presence" aria-expanded="false">
                    <i class="far fa-calendar-check"></i>
                    <p>
                        Presence
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="nav-presence">
                    <ul class="nav">
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ url('/evaluation/presence/overview') }}">
                                <span class="sidebar-mini">M</span>
                                <span class="sidebar-normal">Overview</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ url('/evaluation/presence') }}">
                                <span class="sidebar-mini">M</span>
                                <span class="sidebar-normal">My Presence</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ url('/evaluation/presence/validate') }}">
                                <span class="sidebar-mini">C</span>
                                <span class="sidebar-normal">Check In</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            
            {{--  Menu: Point of Sales  --}}
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#nav-pos" aria-expanded="false">
                    <i class="fal fa-shopping-cart"></i>
                    <p>
                        Point Of Sales
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="nav-pos">
                    <ul class="nav">
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ url('/pos/sales') }}">
                                <span class="sidebar-mini">S</span>
                                <span class="sidebar-normal">Sales</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ url('/pos/sales/history') }}" target="_blank">
                                <span class="sidebar-mini">SH</span>
                                <span class="sidebar-normal">Sales History</span>
                            </a>
                        </li>

                        <li class="nav-item ">
                            <a class="nav-link" href="{{ url('/pos/sales/report') }}" target="_blank">
                                <span class="sidebar-mini">SP</span>
                                <span class="sidebar-normal">Sales Report</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            {{--  Menu: Warehouse  --}}
            @if($group_id == 12 || $group_id == 4 || $group_id == 5 || $group_id == 6 || $group_id == 7 || $group_id == 8)
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#nav-mastertable" aria-expanded="false">
                    <i class="fal fa-archive"></i>
                    <p>
                        Warehouse
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="nav-mastertable">
                    <ul class="nav">
                        
                        <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/category') }}">
                            <span class="sidebar-mini">C</span>
                            <span class="sidebar-normal">Category</span>
                        </a>
                        </li>
                        <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/products') }}">
                            <span class="sidebar-mini">P</span>
                            <span class="sidebar-normal">Product</span>
                        </a>
                        </li>
                    </ul>
                </div>
            </li>
            @endif
        </ul>
    </div>
</div>