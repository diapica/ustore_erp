<!DOCTYPE HTML>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Ustore ERP</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'
	/>
	<link href="{{ asset('assets/styles/fontawesome-all.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/styles/bootstrap.min.css') }}" rel="stylesheet" />

	<link href="{{ asset('assets/plugins/animated-icons/animated-icons.css') }}" rel="stylesheet" />

	<link href="{{ asset('assets/styles/style.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/styles/bridge.css') }}" rel="stylesheet" />
</head>

<body>
	<meta name="csrf-token" content="{{ csrf_token() }}">

	@include('globals.topnav')
	
	<div class="wrapper">
		@yield('contents')
	</div>

	@include('globals.footer')
</body>

<script type="text/javascript" src=" {{ asset('/assets/plugins/jquery/jquery.3.2.1.min.js') }} "></script>
<script type="text/javascript" src=" {{ asset('/assets/plugins/jquery/popper.min.js') }} "></script>
<script type="text/javascript" src=" {{ asset('/assets/plugins/jquery/bootstrap.min.js') }} "></script>
<script>
  
</script>
@yield('scripts')

</html>