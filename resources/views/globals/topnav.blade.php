<div class="ustore-business-bar">
    <div class="row">
        <div class="col-md-6 text-left">
            <p>
                <a href="#">Knowledge Base</a> | 
                <a href="#">Contact Support</a> | 
                <a href="#">Ustore Home</a> | 
                <a href="/accounts/logout">Logout</a>
            </p>
        </div>
        <div class="col-md-6 text-right">
            <p>Welcome Back, <strong>Jeremias Rama Wijaya</strong></p>
        </div>
    </div>
</div>
<div class="ustore-navigation">
    <div class="logo">
    </div>
    <div class="menu">
        <ul>
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Profile Settings</a></li>
            <li><a href="#">Supervisor</a></li>
            <li><a href="#">Treasurer</a></li>
            <li><a href="#">Secretary</a></li>
            <li><a href="#">Warehouse</a></li>
            <li><a href="#">Merchandising</a></li>
            <li><a href="#">Marketing & Publication</a></li>
            <li><a href="#">Software Development</a></li>
            <li><a href="#">Design</a></li>
        </ul>
    </div>
</div>