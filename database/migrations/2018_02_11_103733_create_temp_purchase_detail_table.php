<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempPurchaseDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_purchase_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('temp_purchase_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('qty');
            $table->double('price');
            $table->text('meta')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('temp_purchase_id')->references('id')->on('temp_purchase');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_purchase_detail');
    }
}
