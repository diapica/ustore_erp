<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_order_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('temp_order_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('qty');
            $table->double('price');
            $table->text('meta')->nullable();
            $table->timestamps();
            $table->foreign('temp_order_id')->references('id')->on('temp_order');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_order_detail');
    }
}
