<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('barcode', 13)->nullable();
            $table->string('product_name',250);
            $table->string('image');
            $table->double('price');
            $table->text('desc')->nullable();
            $table->integer('vendor_id')->unsigned()->default(1);
            $table->integer('category_id')->unsigned()->default(1);
            $table->integer('stock');
            $table->integer('status');
            $table->text('meta')->nullable();
            $table->timestamps();
            $table->foreign('vendor_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
